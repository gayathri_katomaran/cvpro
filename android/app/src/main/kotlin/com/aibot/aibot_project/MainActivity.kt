package com.aibot.aibot_project

import java.io.ByteArrayInputStream
import java.io.File
import java.io.FileOutputStream

class MainActivity : FlutterActivity() {
    fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GeneratedPluginRegistrant.registerWith(getFlutterEngine())
        MethodChannel(
            getFlutterEngine().getDartExecutor().getBinaryMessenger(),
            MainActivity.Companion.BATTERY_CHANNEL
        ).setMethodCallHandler(
            object : MethodCallHandler() {
                fun onMethodCall(call: MethodCall, result: Result) {
                    if (call.method.equals("saveImage")) {
                        val imageTask = SaveImageTask(call.argument("path").toString() + "")
                        SaveImageTask().execute(call.argument("plane") as ByteArray)
                        result.success("true")
                    } else {
                        result.notImplemented()
                    }
                }
            })
    }

    private fun saveImage(planes: ByteArray?, path: String): Boolean {
        val startTime = System.currentTimeMillis()
        val imageStream = ByteArrayInputStream(planes)
        val bitmap: Bitmap = BitmapFactory.decodeStream(imageStream)
        val fileDir: File =
            File(getApplicationContext().getExternalCacheDir().toString() + "/images/")
        if (!fileDir.exists()) {
            fileDir.mkdirs()
        }
        val file: File = File(
            getApplicationContext().getExternalCacheDir().toString() + "/images/",
            System.currentTimeMillis().toString() + ".jpeg"
        )
        val success = false
        try {
            FileOutputStream(file).use { out ->
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out) // bmp is your Bitmap instance
                Log.e("File write time ", (System.currentTimeMillis() - startTime).toString() + "")
                return true
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return success
    }

    internal inner class SaveImageTask(private val path: String) :
        AsyncTask<ByteArray?, Void?, Boolean?>() {
        protected fun doInBackground(vararg params: ByteArray?): Boolean {
            return saveImage(params[0], path)
        }

        protected fun onPostExecute(bitmap: Boolean) {
            Log.e("xxxxxxxx", bitmap.toString() + "")
        }
    }

    companion object {
        private const val BATTERY_CHANNEL = "beingRD.flutter.io/image"
    }
}