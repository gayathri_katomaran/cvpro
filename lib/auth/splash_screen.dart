
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../util/color_constants.dart';
import '../util/image_constants.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  String screen = "Splash";


  startTime() async {

    return Timer(const Duration(seconds: 5), navigationPage);
  }

  void navigationPage() async {
    Get.offAndToNamed("/dashboard");
  }

  @override
  void initState() {
    super.initState();

    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        backgroundColor:whiteColor,
          body: Center(
            child: Image.asset(splash),
          ),),
    );
  }
}
