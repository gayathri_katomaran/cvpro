import 'dart:async';

import 'package:aibot_project/util/string_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:localstorage/localstorage.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:provider/provider.dart';

import '../util/MQTTManager.dart';
import '../util/color_constants.dart';
import '../util/image_constants.dart';
import '../util/state/MQTTAppState.dart';
import '../widget/button_widget.dart';
import '../widget/text_widget.dart';


class BotConnectionScreen extends StatefulWidget {
  const BotConnectionScreen({Key? key}) : super(key: key);

  @override
  State<BotConnectionScreen> createState() => _BotConnectionScreenState();
}

class _BotConnectionScreenState extends State<BotConnectionScreen> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController ipController=TextEditingController();
  TextEditingController nameController=TextEditingController();
  TextEditingController passwordController=TextEditingController();
  TextEditingController topicController=TextEditingController();
  final LocalStorage storage =  LocalStorage('Ai_bot');
  MqttClient? client;
  String?ipAdd,topicValue;

  StreamSubscription? subscription;
  final _isConnected = ValueNotifier<bool>(false);
  MQTTAppState? currentAppState;
  MQTTManager? manager;

  String clientIdentifier = 'lamhx';



  @override
  void initState() {
    super.initState();
    ipAdd = storage.getItem("IP_ADDRESS");
    topicValue = storage.getItem("TOPIC");
    // manager?.disconnect();
  }

  @override
  Widget build(BuildContext context) {
    final MQTTAppState appState = Provider.of<MQTTAppState>(context);
    // Keep a reference to the app state.
    currentAppState = appState;

    if( currentAppState?.getAppConnectionState==MQTTAppConnectionState.connected||currentAppState?.getAppConnectionState==MQTTAppConnectionState.connecting){
      _isConnected.value=true;
      ipController.text=ipAdd??"";
      topicController.text=topicValue??"";
    }else{
      _isConnected.value=false;
    }
    return Container(
      color: primaryColor,
      child: SafeArea(
        child:  GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            body: Stack(
              fit: StackFit.expand,
              children: [
                Container(
                  color: Colors.white,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child:Column(
                    children: [
                      _buildConnectionStateText(
                          _prepareStateMessageFrom(currentAppState?.getAppConnectionState)),
                      Form(
                        key: _formKey,
                        child: Padding(
                          padding: const EdgeInsets.only(top:20.0,left: 16,right: 16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children:  [

                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding:  const EdgeInsets.all(8.0),
                                    child: InkWell(
                                        highlightColor: Colors.transparent,
                                        onTap: (){
                                          if(_isConnected.value){
                                            Get.back(result: 1);
                                          }else{
                                            Get.back();

                                          }
                                        }
                                        ,child: SvgPicture.asset(backBtn,fit: BoxFit.contain,)),
                                  ),
                                  const Padding(
                                    padding: EdgeInsets.only(top:12.0,left: 8,right: 8,bottom: 8),
                                    child: TextWidget(
                                      text: connectTxt,
                                      size: 22,
                                      color:darkColor,
                                      weight: FontWeight.w800,
                                      maxLines: 2,
                                    ),
                                  ),
                                ],
                              ),


                              Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: TextFormField(
                                  controller: ipController,
                                  textInputAction: TextInputAction.next,
                                  readOnly: currentAppState?.getAppConnectionState == MQTTAppConnectionState.connected?true:false,
                                  decoration:  const InputDecoration(
                                    labelText: "Ip Address",
                                    labelStyle: TextStyle(
                                      fontFamily: 'Montserrat',
                                      color: Colors.grey,
                                      fontSize: 14,
                                    ),
                                    border:  OutlineInputBorder(
                                      borderSide: BorderSide.none,
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                    ),
                                    enabledBorder:  OutlineInputBorder(
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                      borderSide: BorderSide(color: lightGreyColor),
                                    ),
                                    focusedBorder:  OutlineInputBorder(
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                      borderSide: BorderSide(color: lightGreyColor),
                                    ),
                                    focusedErrorBorder:  OutlineInputBorder(
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                      borderSide: BorderSide(color: lightGreyColor),
                                    ),
                                    errorBorder:  OutlineInputBorder(
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                      borderSide: BorderSide(color: lightGreyColor),
                                    ),
                                    fillColor: primaryColor,),
                                  keyboardType: TextInputType.text,
                                  onSaved: (String? val) {
                                    setState(() {
                                      ipController.text=val!;
                                      // loginOTPRequest?.username = val!;
                                      // mobileNo=val;
                                    });
                                  },
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: TextFormField(
                                  controller: topicController,
                                  textInputAction: TextInputAction.done,
                                  readOnly: currentAppState?.getAppConnectionState == MQTTAppConnectionState.connected?true:false,
                                  decoration:  const InputDecoration(
                                    labelText: "Topic",
                                    labelStyle: TextStyle(
                                      fontFamily: 'Montserrat',
                                      color: Colors.grey,
                                      fontSize: 14,
                                    ),
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide.none,
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                      borderSide: BorderSide(color: lightGreyColor),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                      borderSide: BorderSide(color: lightGreyColor),
                                    ),
                                    focusedErrorBorder: OutlineInputBorder(
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                      borderSide: BorderSide(color: lightGreyColor),
                                    ),
                                    errorBorder: OutlineInputBorder(
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                      borderSide: BorderSide(color: lightGreyColor),
                                    ),
                                    fillColor: primaryColor,),
                                  keyboardType: TextInputType.text,
                                  onSaved: (String? val) {
                                    setState(() {
                                      topicController.text=val!;
                                      // loginOTPRequest?.username = val!;
                                      // mobileNo=val;
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      ValueListenableBuilder<bool>(
                        valueListenable: _isConnected,
                        builder: (context, isConnected, child) {
                          return Padding(
                            padding:  const EdgeInsets.only(top:8.0,bottom: 16),
                            child: ButtonWidget(
                              width: MediaQuery.of(context).size.width / 1.4,
                              text: "Connect",
                              size: 16,
                              txtColor: Colors.white,
                              color: primaryColor,onPressed:isConnected ||ipController.text.isEmpty || topicController.text.isEmpty ? null : _configureAndConnect,

                            ),
                          );
                        },
                      ),

                      /* ValueListenableBuilder<bool>(
                        valueListenable: _isConnected,
                        builder: (context, isConnected, child) {
                          return Padding(
                            padding:  const EdgeInsets.only(top:8.0,bottom: 16),
                            child: ButtonWidget(
                              width: MediaQuery.of(context).size.width / 1.4,
                              text: "DisConnect",
                              size: 16,
                              txtColor: Colors.white,
                              color: primaryColor,onPressed:!isConnected? null : _configureAndConnect,

                            ),
                          );
                        },
                      )
                      */

                      Padding(
                        padding:  const EdgeInsets.only(top:8.0,bottom: 16),
                        child: ButtonWidget(
                            width: MediaQuery.of(context).size.width / 1.4,
                            text: "DisConnect",
                            size: 16,
                            txtColor: Colors.white,
                            color: primaryColor,onPressed: currentAppState?.getAppConnectionState == MQTTAppConnectionState.connected?disConnect:null
                        ),
                      )
                    ],
                  ),
                ),

              ],),
          ),
        ),

      ),
    );
  }
  disConnect(){
    manager?.disconnect();
    manager?.onDisconnected();
    client?.disconnect();
    setState(() {
      currentAppState?.setAppConnectionState(MQTTAppConnectionState.disconnected);
    });
    Get.back();
  }

  Widget _buildConnectionStateText(String status) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
              color: status=="Disconnected"?Colors.red:Colors.green,
              child: Padding(
                  padding: const EdgeInsets.only(top: 4.0,bottom: 4),
                  child: TextWidget(
                      text: status,
                      size: 16,
                      color: darkColor,
                      align: TextAlign.center,
                      weight: FontWeight.w500)
              )),
        ),
      ],
    );
  }
  // Utility functions
  _prepareStateMessageFrom(MQTTAppConnectionState? state) {
    switch (state) {
      case MQTTAppConnectionState.connected:
        debugPrint("Connected Toast");
        // Get.back(result: 1);
        return 'Connected';
      case MQTTAppConnectionState.connecting:
        return 'Connecting';
      case MQTTAppConnectionState.disconnected:
        return 'Disconnected';
    }
  }

  _configureAndConnect() {
    storage.setItem("IP_ADDRESS", ipController.text.trim());
    storage.setItem("USER_NAME", nameController.text.trim());
    storage.setItem("PASSWORD", passwordController.text.trim());
    storage.setItem("TOPIC", topicController.text.trim());
    String osPrefix = 'Flutter_iOS';
    manager = MQTTManager(
      host:ipController.text.trim(),
      topic:topicController.text.trim(),
      identifier: osPrefix, state: currentAppState!,
    );
    manager?.initializeMQTTClient();
    manager?.connect("Cvpro","Cvpro");
    Get.back(result: 1);
  }
  /// The subscribed callback
  void onSubscribed(String topic) {
    debugPrint('EXAMPLE::Subscription confirmed for topic $topic');
  }
}
