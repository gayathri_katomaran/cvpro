import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ffi';
import 'dart:ui' as ui;
import 'package:aibot_project/widget/snack_bar.dart';
import 'package:archive/archive_io.dart';
import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:mqtt_client/mqtt_server_client.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../models/excel_list.dart';
import '../util/available_cameras.dart';
import '../util/color_constants.dart';
import '../util/image_constants.dart';
import '../util/state/MQTTAppState.dart';
import '../util/string_constants.dart';
import '../widget/cupertino_switch.dart';
import '../widget/text_widget.dart';
import 'package:image/image.dart' as imglib;

typedef convert_func = Pointer<Uint32> Function(Pointer<Uint8>, Pointer<Uint8>, Pointer<Uint8>, Int32, Int32, Int32, Int32);
typedef Convert = Pointer<Uint32> Function(Pointer<Uint8>, Pointer<Uint8>, Pointer<Uint8>, int, int, int, int);

class DataCollectionScreen extends StatefulWidget {
  const DataCollectionScreen({Key? key}) : super(key: key);

  @override
  State<DataCollectionScreen> createState() => _DataCollectionScreenState();
}

class _DataCollectionScreenState extends State<DataCollectionScreen>  with WidgetsBindingObserver{
  CameraController? controller;
  CameraImage? cameraImage;
  bool isCamera = false,
      isData = false,
      isBot = false,
      isFlash = false,
      isMotor = false;
  late Throttler throttler;

  List<ExcelList> excelList=[];
  MQTTAppState? currentAppState;
  MqttServerClient? _client;
  String controlValue="",leftValue="",rightValue="";
  String fileName="";
  List<String> imageList=[];
  List<String> imageNameList=[];
  final resolutionPresets = ResolutionPreset.values;
  ResolutionPreset currentResolutionPreset = ResolutionPreset.low;
  bool _isCameraInitialized = false;
  TextEditingController valueController = TextEditingController();
  double minAvailableZoom = 1.0;
  double maxAvailableZoom = 2.0;
  double currentZoomLevel = 1.0;
  FlashMode? _currentFlashMode;
  Directory? appDirectory;
  String? pictureDirectory="", excelDirectory="", mainDirectory="",subDirectory="";
  int pictureCount = 0,folderCount=0;
   Directory? datasetDir;
  Timer? _timer;
  Uint8List? imageData;

  final DynamicLibrary convertImageLib = Platform.isAndroid
      ? DynamicLibrary.open("libconvertImage.so")
      : DynamicLibrary.process();
  late Convert conv;

  @override
  void initState() {
    super.initState();
    throttler = Throttler(milliSeconds: 160);
    initCamera(cameras![0]);
    NetworkUtils.checkConnectivity();
    NetworkUtils.checkInternetConnection();

    NetworkUtils.startTimer();
    conv = convertImageLib.lookup<NativeFunction<convert_func>>('convertImage').asFunction<Convert>();
  }

  void initCamera(CameraDescription cameraDescription) async {
    double x = 0.5;
    double y = 0.5;
    Offset focusPoint = Offset(x, y);
    // Instantiating the camera controller
    final CameraController cameraController = CameraController(
      cameraDescription,
      currentResolutionPreset,
      enableAudio: false,
      imageFormatGroup: ImageFormatGroup.yuv420,
    );

    // Replace with the new controller
    if (mounted) {
      setState(() {
        controller = cameraController;
      });
    }

    // Update UI if controller updated
    cameraController.addListener(() {
      if (mounted) setState(() {});
    });

    // Initialize controller
    try {
      await cameraController.initialize().then((value) async{
        cameraController.lockCaptureOrientation(DeviceOrientation.portraitUp);
        if (!mounted) {
          return;
        }

        setState(() {});
        controller?.getMaxZoomLevel().then((value) => maxAvailableZoom = value);

        controller?.getMinZoomLevel().then((value) => minAvailableZoom = value);
        _currentFlashMode = controller!.value.flashMode;
   /*
        controller?.setExposurePoint(focusPoint);
        controller?.setFocusPoint(focusPoint);*/
        // controller?.setExposureMode(ExposureMode.auto);
        // controller?.setFocusMode(FocusMode.auto);
      });
    } on CameraException catch (e) {
      if (e is CameraException) {
        switch (e.code) {
          case 'CameraAccessDenied':
            debugPrint('User denied camera access.');
            break;
          default:
            debugPrint('Handle other errors.');
            break;
        }
      }
    }

    // Update the Boolean
    if (mounted) {
      setState(() {
        _isCameraInitialized = controller!.value.isInitialized;
      });
    }
  }
  void takePicture()  async{

    try {

    /*  final previewSize = controller?.value.previewSize;

      // Calculate the FocusPoint area based on the preview size
      final focusPointArea = Rect.fromCenter(
        center: Offset(previewSize!.width / 2, previewSize.height / 2),
        width: previewSize.width / 2,
        height: previewSize.height / 2,
      );


      // Crop the image to the FocusPoint area
      final bytes = await picture.readAsBytes();
      final codec = await ui.instantiateImageCodec(bytes);
      final frame = await codec.getNextFrame();
      final cropped = await frame.image.toByteData(
        format: ui.ImageByteFormat.png,
      );*/
     /* final XFile picture = await controller!.takePicture();
      debugPrint("picture:${picture.path}");*/
       controller?.startImageStream((picture) async{

         throttler.run(() async {
           try {
             debugPrint("Format:${picture.format.group} ${picture.planes} ${picture.planes[0].bytes}");
             debugPrint("cameraImage_Width:${picture.height} ${picture.width}");

             imglib.Image? img;
             if (picture.format.group == ImageFormatGroup.yuv420) {

               img = convertYUV420ToImage(picture);
             }
             /*  else if (picture.format.group == ImageFormatGroup.bgra8888) {
           img = _convertBGRA8888(picture);
         }*/

             imglib.PngEncoder pngEncoder = imglib.PngEncoder();
             // Convert to png
             List<int> png = pngEncoder.encodeImage(img!);
             debugPrint("png:$png");

             // stop the image stream to capture a single photo

             debugPrint("picture${picture.format.group} ");

             // Get the directory where you want to save the picture
             appDirectory = await getExternalStorageDirectory();

             mainDirectory='${appDirectory?.parent.parent.parent.parent.path}/CVPro-APP/dataSet';
             datasetDir = Directory('$mainDirectory');
             debugPrint("appDirectory$datasetDir  $mainDirectory");

             pictureDirectory = '$mainDirectory/$folderCount/Images';

             await Directory(pictureDirectory!).create(recursive: true);
             // Create a file with a unique name based on the current timestamp
             final String imageName = 'Image-${++pictureCount}';
             final String filePath = '$pictureDirectory/$imageName.png';
             final directoryComponents = filePath.split('/');
             final folderName = directoryComponents[directoryComponents.length - 3];
             final imageNames = directoryComponents[directoryComponents.length - 2];
             final imagePath = '$folderName/$imageNames/$imageName';
             debugPrint("imagePath:${imagePath}");

             setState(() {
               imageList.add(imagePath);
               imageNameList=imageList.toSet().toList();
             });

             if(isData==true && isBot==true){
               for(int i=0; i<imageNameList.length;i++){
                 setState(() {
                   excelList.add(ExcelList(imageNameList[i], leftValue, rightValue));
                   debugPrint("ImageList:${imageNameList[i]}");
                   createExcelFile(mainDirectory,folderCount,excelList);

                 });

               }
             }else if(isData==true && isBot==false){
               debugPrint("imageList_length:${imageList.length}${imageList.length}");

               for(int i=0; i<imageNameList.length;i++){
                 setState(() {
                   excelList.add(ExcelList(imageNameList[i], "", ""));
                   createExcelFile(mainDirectory,folderCount,excelList);
                   debugPrint("ImageList1:${excelList[i].imageName}");
                 });
               }
             }
             debugPrint("imageList:${imageNameList.length}");
             // fileName=file.name;
             debugPrint("excelList_length:${excelList.length}  $imageName $pictureCount");
             var startTime = DateTime.now();

             debugPrint("lastThreeComponents:$imagePath");
             debugPrint("filepath__$filePath");
             final File file = File(filePath);
             String basename = p.basename(file.path);
             debugPrint("basename:$basename");
             // // Write the picture data to the file
             // final List<int> bytes = await picture.planes.rea;

             file.writeAsBytesSync(imglib.encodeJpg(img,quality: 75));
             var endTime = DateTime.now();
             var getTime=endTime.difference(startTime).inMilliseconds;

             debugPrint('Time taken to save file: $getTime ms');
             debugPrint("imageName:$imageName $isData $isBot");


           } on PlatformException catch (e) {
             debugPrint(
                 "==== checkLiveness Method is not implemented ${e.message}");
           }
         });


        return picture;
      });




    } on PlatformException catch (e) {
      final XFile picture = await controller!.takePicture();
      debugPrint("picture1:${picture.path}");

      debugPrint("Exception:${e.toString()}");
      snackBarAlert(e.toString());
      return null;
    }
  }



  imglib.Image convertYUV420ToImage(CameraImage cameraImage) {

    Stopwatch stopwatch = Stopwatch();
    stopwatch.start();
    var img = imglib.Image(cameraImage.width, cameraImage.height); // Create Image buffer

    final int width = cameraImage.width;
    final int height = cameraImage.height;
    final int uvRowStride = cameraImage.planes[1].bytesPerRow;
    final int? uvPixelStride = cameraImage.planes[1].bytesPerPixel;
    const shift = (0xFF << 24);

    for(int x=0; x < width; x++) {
      for(int y=0; y < height; y++) {
        final int uvIndex = uvPixelStride! * (x/2).floor() + uvRowStride*(y/2).floor();
        final int index = y * width + x;

        final yp = cameraImage.planes[0].bytes[index];
        final up = cameraImage.planes[1].bytes[uvIndex];
        final vp = cameraImage.planes[2].bytes[uvIndex];
        // Calculate pixel color
        int r = (yp + vp * 1436 / 1024 - 179).round().clamp(0, 255);
        int g = (yp - up * 46549 / 131072 + 44 -vp * 93604 / 131072 + 91).round().clamp(0, 255);
        int b = (yp + up * 1814 / 1024 - 227).round().clamp(0, 255);
        // color: 0x FF  FF  FF  FF
        //           A   B   G   R
        img.data[index] = shift | (b << 16) | (g << 8) | r;
        stopwatch.stop();
        int getImgDataTime = stopwatch.elapsedMilliseconds;
        // debugPrint('get imgData: ${getImgDataTime}ms, imglib.Image.fromBytes: ${stopwatch.elapsedMilliseconds}ms');
      }
    }

    return img;
   /* Stopwatch stopwatch = Stopwatch();
    stopwatch.start();

    Pointer<Uint8> p = malloc.allocate( cameraImage.planes[0].bytes.length);
    Pointer<Uint8> p1 = malloc.allocate( cameraImage.planes[1].bytes.length);
    Pointer<Uint8> p2 = malloc.allocate( cameraImage.planes[2].bytes.length);

    // Assign the planes data to the pointers of the image
    Uint8List pointerList = p.asTypedList(cameraImage.planes[0].bytes.length);
    Uint8List pointerList1 = p1.asTypedList(cameraImage.planes[1].bytes.length);
    Uint8List pointerList2 = p2.asTypedList(cameraImage.planes[2].bytes.length);
    pointerList.setRange(0, cameraImage.planes[0].bytes.length, cameraImage.planes[0].bytes);
    pointerList1.setRange(0, cameraImage.planes[1].bytes.length, cameraImage.planes[1].bytes);
    pointerList2.setRange(0, cameraImage.planes[2].bytes.length, cameraImage.planes[2].bytes);

    // Call the convertImage function and convert the YUV to RGB
    Pointer<Uint32> imgP = conv(p, p1, p2, cameraImage.planes[1].bytesPerRow,
        cameraImage.planes[1].bytesPerPixel!, cameraImage.planes[0].bytesPerRow, cameraImage.height);

    // Get the pointer of the data returned from the function to a List
    List<int> imgData = imgP.asTypedList((cameraImage.planes[0].bytesPerRow * cameraImage.height));

    stopwatch.stop();
    int getImgDataTime = stopwatch.elapsedMilliseconds;

    stopwatch.reset();
    stopwatch.start();
    // Generate image from the converted data
    imglib.Image img;
    img = imglib.Image.fromBytes(cameraImage.height, cameraImage.planes[0].bytesPerRow, imgData);

    // Free the memory space allocated
    // from the planes and the converted data
    malloc.free(p);
    malloc.free(p1);
    malloc.free(p2);
    malloc.free(imgP);

    stopwatch.stop();
    debugPrint('get imgData: ${getImgDataTime}ms, imglib.Image.fromBytes: ${stopwatch.elapsedMilliseconds}ms');

    return img;*/
  }

  void _startTakingPictures() async{
    setState(() {
      isData = true;
      folderCount++;
      pictureCount=0;
    });
    if (controller?.value.isStreamingImages == true) {
      try {
        await controller?.stopImageStream();
      } catch (e) {
        // handle error if any
      }
    }
    takePicture();

    /*_timer= Timer.periodic(const Duration(sec:50), (_) {
      if (isData && _isCameraInitialized) {
      }
    });*/
  }

  Future<void> toggleIsData() async {
    if (isData) {
      // initRecorder();
      imageList.clear();
      imageNameList.clear();
      excelList.clear();
      bool permission = await requestPermission();
      if (Platform.isAndroid) {
        if (permission) {
          _startTakingPictures();
        }
        else{
          snackBarAlert("Please give necessary permissions");
        }
      }else{
        _startTakingPictures();

      }

    } else {
      if (controller?.value.isStreamingImages == true) {
        try {
          await controller?.stopImageStream();
        } catch (e) {
          // handle error if any
        }
      }else{
        await controller?.stopImageStream();
      }
      debugPrint("createZipFile");
    }
  }

  void createZipFile(Directory dir,) async{
    // Create a new archive
    final zipFile = ZipFileEncoder();
    final formatter = DateFormat('yyyyMMdd_HHmmss');
    final zipName = formatter.format(DateTime.now());
    appDirectory = await getExternalStorageDirectory();

    final zipPath = '${appDirectory?.path}/$zipName.zip';
    // Add the contents of the directory to the archive
    zipFile.create(zipPath);
    debugPrint("zipFile:$zipFile $dir");
    dir.listSync(recursive: true).forEach((entity) {
      if (entity is File) {
        final archiveFile = File(entity.path);
        final archiveName = '$zipName/${dir.path.split('/').last}/${archiveFile.path.substring(dir.path.length + 1, archiveFile.path.length)}';
        debugPrint("archiveFile:$archiveFile  $archiveName");
        zipFile.addFile(archiveFile, archiveName,);
      }
    });
    // Close the archive
    zipFile.close();
    dir.deleteSync(recursive: true);
    Get.close(2);

  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    final CameraController? cameraController = controller;

    // App state changed before we got the chance to initialize.
    if (cameraController == null || !cameraController.value.isInitialized) {
      return;
    }

    if (state == AppLifecycleState.inactive) {
      // Free up memory when camera not active
      cameraController.dispose();
    } else if (state == AppLifecycleState.resumed) {
      // Reinitialize the camera with same properties
      initCamera( cameras![isCamera ? 1 : 0],);
    }
  }

  @override
  void dispose() {
    super.dispose();
    isData=false;
    // controller?.stopImageStream();
    controller?.dispose();
    NetworkUtils.stopTimer();

  }
  Future<bool> _willPopCallback() async {
    bool fileExists = await (datasetDir?.exists() ?? Future.value(false));

    if (fileExists) {
      // subfolder does not exist
      confirmDialog();
    }else{
      debugPrint("Not folder");
      Get.back();
    }
    return false; // return true if the route to be popped
  }

  @override
  Widget build(BuildContext context) {

    final MQTTAppState appState = Provider.of<MQTTAppState>(context);
    // Keep a reference to the app state.
    currentAppState = appState;
    debugPrint("currentAppState:$currentAppState");


    if(isBot==true){
      if( NetworkUtils.hasInternet) {
        controlValue = currentAppState!.getReceivedText;
        debugPrint("controlValue__:$controlValue");
        if (currentAppState!.getReceivedText.isNotEmpty) {
          setState(() {
            leftValue = currentAppState!
                .getReceivedText
                .split(",")
                .first;
            rightValue = currentAppState!
                .getReceivedText
                .split(",")
                .last;
            debugPrint("values$leftValue $rightValue");
          });
        } else {
          leftValue = "";
          rightValue = "";
        }
      }else{
        snackBarAlert("Mqtt Disconnected");
        controlValue="";
      }
    }else{
      leftValue="";
      rightValue="";

    }


    ui.Size size = MediaQuery.of(context).size;
    debugPrint("preset:${resolutionPresets[0].name}");

    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Container(
          color: primaryColor,
          child: SafeArea(
              child: Scaffold(
                backgroundColor: whiteColor,
                body: SlidingUpPanel(
                  defaultPanelState: PanelState.CLOSED,
                  minHeight: 60,
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(18.0),
                      topRight: Radius.circular(18.0)),
                  parallaxEnabled: true,
                  panelBuilder: (scrollController) => buildSlidingPanel(
                      scrollController: scrollController, size: size,status:currentAppState),
                  body: Stack(
                    children: [

                      (!controller!.value.isInitialized)
                          ? Container()
                          :Positioned(
                          top: 0.0,
                          left: 0.0,
                          width: size.width,
                          height: size.height,
                          child: SizedBox(
                              width: size.width,
                              height: size.height / controller!.value.aspectRatio,
                              child: AspectRatio(
                                aspectRatio: controller!.value.aspectRatio,
                                child: CameraPreview(controller!),
                              ))
                      ),
                       Align(
                          alignment: Alignment.topLeft,
                          child:Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: InkWell(
                                    highlightColor: Colors.transparent,
                                    onTap: ()async{
                                      bool fileExists = await (datasetDir?.exists() ?? Future.value(false));

                                      if (fileExists) {
                                      // subfolder does not exist
                                        confirmDialog();
                                      }else{
                                        debugPrint("Not folder");
                                        Get.back();
                                      }

                                    }
                                    ,child: SvgPicture.asset(backBtn,fit: BoxFit.contain,)),
                              ),
                              const Padding(
                                padding: EdgeInsets.only(top:12.0,left: 8,right: 8,bottom: 8),
                                child: TextWidget(
                                  text:dataTxt,
                                  size: 22,
                                  color:whiteColor,
                                  weight: FontWeight.w800,
                                  maxLines: 2,
                                ),
                              ),
                            ],
                          ),)
                    ],
                  ),
                ),
              ))),
    );
  }

  confirmDialog(){
    return  showCupertinoDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext ctx) {
          return CupertinoTheme(
            data: const CupertinoThemeData(primaryColor: whiteColor,barBackgroundColor: whiteColor,
              brightness: Brightness.light
            ),
            child: CupertinoAlertDialog(
              title: const Padding(
                padding: EdgeInsets.all(8.0),
                child: TextWidget(
                  text: "Convert to Zip",
                  color: darkColor,
                  weight: FontWeight.w700,
                  size: 16,
                  align: TextAlign.center,
                ),
              ),
              content: SizedBox(
                width: Get.width/2,
                child: const TextWidget(
                  text: "Do you want to convert the folder to a zip file?",
                  color: darkColor,
                  maxLines: 2,
                  weight: FontWeight.w400,
                  size: 16,
                  align: TextAlign.center,
                ),
              ),
              actions: [
                CupertinoDialogAction(
                  onPressed: () {
                    Get.close(2);
                  },
                  isDefaultAction: true,
                  isDestructiveAction: true,
                  child: const TextWidget(
                    text: "Cancel",
                    size: 16,
                    color:Colors.red, weight: FontWeight.w500,
                  ),
                ),
                CupertinoDialogAction(
                  onPressed: () {
                    if(Platform.isAndroid){
                      createZipFromDirectory(mainDirectory!);

                    } else{
                      createZipFile(datasetDir!);

                    }
                  },
                  isDefaultAction: false,
                  isDestructiveAction: false,
                  child: const TextWidget(
                    text: "Convert",
                    size: 16,
                    color:primaryColor, weight: FontWeight.w500,
                  ),
                )
              ],
            ),
          );
        });

  }
  Future<void> createZipFromDirectory(String directoryPath) async {
    final dir = Directory(directoryPath);
    final parentDir = dir.parent;
     debugPrint("parentDir:$parentDir");
    final formatter = DateFormat('yyyyMMdd_HHmmss');
    final zipName = formatter.format(DateTime.now());

    final zipFileName = '$zipName.zip';
    final zipFilePath = p.join(parentDir.path, zipFileName);

    final encoder = ZipFileEncoder();
    encoder.create(zipFilePath);

    for (final entity in dir.listSync(recursive: true)) {
      if (entity is File) {
        final relativePath = p.relative(entity.path, from: dir.path);
        encoder.addFile(File(entity.path), relativePath);
      }
    }

    encoder.close();
    dir.deleteSync(recursive: true);
    Get.close(2);

  }

  buildSlidingPanel(
      {required ScrollController scrollController, required ui.Size size, MQTTAppState? status}) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: PreferredSize(
        preferredSize: const ui.Size.fromHeight(40),
        child: AppBar(
          title: const Center(
              child: Icon(
                Icons.drag_handle,
                color: darkColor,
              )),
          automaticallyImplyLeading: false,
          backgroundColor: whiteColor,
          elevation: 0,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0))),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: size.width / 2.2,
                    child: CupertinoWidget(
                      text: data,
                      isValue: isData,
                      onChange: (bool? val) async{
                        setState(() {
                          isData = val!;
                        });
                        await toggleIsData();
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: size.width / 2.2,
                    child: CupertinoWidget(
                      text: botTxt,
                      isValue: isBot,
                      onChange: (bool? val) {
                        setState(() {
                          isBot = val!;
                        });
                        if(isBot==false){
                          _client?.unsubscribe("cvPro");
                        }
                      },
                    ),
                  ),
                )
              ]),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: size.width / 2.2,
                    child: CupertinoWidget(
                      text: cameraTxt,
                      isValue: isCamera,
                      onChange: (bool? val) async{
                        setState(() {
                          _isCameraInitialized = false;
                        });
                        initCamera(
                          cameras![isCamera ? 0 : 1],
                        );
                        setState(() {
                          isCamera = !isCamera;
                        });
                        await controller?.setFlashMode(
                        FlashMode.off,
                        );
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: size.width / 2.2,
                    child: CupertinoWidget(
                      text: flashTxt,
                      isValue: isFlash,
                      onChange: (bool? val) async{
                        setState(() {
                          isFlash = val!;
                        });
                        if(isFlash){
                          setState(() {
                            _currentFlashMode = FlashMode.torch;

                          });
                          await controller?.setFlashMode(
                            FlashMode.torch,
                          );
                        }else{
                          setState(() {
                            _currentFlashMode = FlashMode.off;

                          });
                          await controller?.setFlashMode(
                            FlashMode.off,
                          );
                        }
                      },
                    ),
                  ),
                )
              ]),
          Padding(
            padding: const EdgeInsets.only( top: 8,left: 32,right: 16),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,

                children: [
                  SizedBox(
                    width: size.width / 3,

                    child: const TextWidget(
                        text: resolutionTxt,
                        size: 16,
                        maxLines: 2,
                        color: darkColor,
                        weight: FontWeight.w500),
                  ),
                  SizedBox(
                    width: size.width / 2,
                    height: size.height / 12,
                    child: InputDecorator(
                      decoration: const InputDecoration(border: OutlineInputBorder(borderSide: BorderSide(color: lightGreyColor),),contentPadding: EdgeInsets.only(left: 12,)),

                      child: DropdownButtonHideUnderline(
                        child: DropdownButton<ResolutionPreset>(
                          dropdownColor: whiteColor,
                          iconEnabledColor: primaryColor,
                          isExpanded: true,
                          isDense: true,
                          underline: Container(),
                          value: currentResolutionPreset,
                          items: [
                            for (ResolutionPreset preset in resolutionPresets)
                              DropdownMenuItem(

                                value: preset,
                                child: TextWidget(
                                    text:
                                    preset.name=="low"?"${preset.toString().split('.')[1].toUpperCase()} (320x240)":
                                    preset.name=="medium"?"${preset.toString().split('.')[1].toUpperCase()} (640x480)":
                                    preset.name=="high"?"${preset.toString().split('.')[1].toUpperCase()} (1280x720)":
                                    preset.name=="veryHigh"?"${preset.toString().split('.')[1].toUpperCase()} (1920x1080)":
                                    preset.name=="ultraHigh"?"${preset.toString().split('.')[1].toUpperCase()} (3840x2160)"
                                        :preset.toString().split('.')[1].toUpperCase(),
                                    size: 12,
                                    color: primaryColor,
                                    weight: FontWeight.w500),
                              )
                          ],
                          onChanged: (value) {
                            setState(() {
                              currentResolutionPreset = value!;
                              _isCameraInitialized = false;
                            });
                            initCamera(controller!.description);
                          },
                          hint: const Text("Select item"),
                        ),
                      ),
                    ),
                  ),
                ]),
          ),
          Padding(
            padding: const EdgeInsets.only( left: 32,right: 8),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const TextWidget(
                      text: controlTxt,
                      size: 16,
                      color: darkColor,
                      weight: FontWeight.w500),

                  Padding(
                    padding:  const EdgeInsets.only(right: 8.0, left: 24),
                    child: SizedBox(
                      width: size.width / 2,
                      child: TextWidget(
                          text:isBot==true?currentAppState!.getReceivedText:"",
                          size: 16,
                          color: primaryColor,
                          align: TextAlign.start,
                          weight: FontWeight.w500),
                    ),
                  ),
                ]),
          ),
        ],
      ),
    );
  }
  createExcelFile(String? mainDirectory, int folderCount, List<ExcelList> excelList,) async {

    // final List<List<ExcelList>> convertList = excelList.map((item) => item.toList()).toList();


    final convertList =[  ['Image Name', 'Control Left','Control Right'],
      ...excelList.map((item) => [item.imageName, item.controlLeft??"", item.controlRight??""]).toList()];


    final printer = ColumnPrinter(convertList, auto: true);
    final csv = printer.print();

    excelDirectory = '$mainDirectory/$folderCount/ExcelFile';
    await Directory(excelDirectory!).create(recursive: true);

    final path = '$excelDirectory/data_collection.csv';
    debugPrint("path:$path");
    final File file = File(path);
    await file.writeAsString(csv);
    excelList.clear();
  }

}
class ColumnPrinter {
  final List<List<dynamic>> data;
  final bool auto;
  final List<int>? widths;

  ColumnPrinter(this.data, {this.auto = false, this.widths});

  String print() {
    final output = StringBuffer();

    if (auto) {
      final maxLengths = _calculateColumnWidths();
      for (var row in data) {
        for (var i = 0; i < row.length; i++) {
          final cell = row[i].toString();
          final width = maxLengths[i];
          output.write(cell.padRight(width));
          output.write(', ');
        }
        output.write('\n');
      }
    } else if (widths != null) {
      for (var row in data) {
        for (var i = 0; i < row.length; i++) {
          final cell = row[i].toString();
          final width = widths![i];
          output.write(cell.padRight(width));
          output.write(', ');
        }
        output.write('\n');
      }
    } else {
      for (var row in data) {
        for (var cell in row) {
          output.write(cell);
          output.write(', ');
        }
        output.write('\n');
      }
    }

    return output.toString();
  }

  List<int> _calculateColumnWidths() {
    final lengths = List.generate(data[0].length, (i) => 0);

    for (var row in data) {
      for (var i = 0; i < row.length; i++) {
        final cell = row[i].toString();
        if (cell.length > lengths[i]) {
          lengths[i] = cell.length;
        }
      }
    }

    return lengths;
  }
}
class Throttler {
  Throttler({required this.milliSeconds});

  final int milliSeconds;

  int? lastActionTime;

  void run(VoidCallback action) {
    if (lastActionTime == null) {
      action();
      lastActionTime = DateTime.now().millisecondsSinceEpoch;
    } else {
      if (DateTime.now().millisecondsSinceEpoch - lastActionTime! >
          (milliSeconds)) {
        action();
        lastActionTime = DateTime.now().millisecondsSinceEpoch;
      }
    }
  }
}