

import 'package:aibot_project/object_detection/live_camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../util/available_cameras.dart';
import '../util/color_constants.dart';
import '../util/image_constants.dart';
import '../util/string_constants.dart';
import '../widget/sos_card_widget.dart';
import '../widget/text_widget.dart';


class DashboardScreen extends StatefulWidget {

    const DashboardScreen({Key? key}) : super(key: key);

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen>  with WidgetsBindingObserver{

    @override
  void initState() {
      super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(color:primaryColor,child: SafeArea(child: Scaffold(
      backgroundColor: whiteColor,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children:  [
            InkWell(
              onTap: (){
                Get.toNamed('/setting');
              },
              child: Padding(
                padding: const EdgeInsets.all(24.0),
                child: Align(alignment:Alignment.topRight,child: SvgPicture.asset(setting,fit: BoxFit.contain,width: 40,height: 40,)),
              ),
            ),
           const Center(
             child: Padding(
              padding: EdgeInsets.all(16.0),
              child: TextWidget(
                text: "AI Bot",
                size: 30,
                color: primaryColor,
                weight: FontWeight.w700,
                align: TextAlign.center,
              ),
          ),
           ),
          const SizedBox(height: 50,),
          Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,

              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 8.0,right: 8,bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SosCard(text: dataTxt,color1: const Color(0xff8677FE), color2:const Color(0xffB7AEFF), txtColor: whiteColor, onPressed:(){
                        Get.toNamed('/collection');
                      }, iconImage: dataImg, ),
                      SosCard(text:objectTxt, color1:const Color(0xffFF7544), color2:const Color(0xffFF9D7B),txtColor: whiteColor, onPressed:(){
                        Get.to(LiveFeed(cameras!));
                        // Get.toNamed('/detect');
                      },iconImage: object,  ),

                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0,right: 8,bottom: 8,top: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SosCard(text: selfTxt,color1: const Color(0xff46A1E0), color2:const Color(0xff6DB9EF), txtColor: whiteColor, onPressed:(){
                        Get.toNamed('/self');
                      }, iconImage: dataImg, ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0,right: 8,top: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                    SosCard(text: classifyTxt,color1:const Color(0xffFB5A7C), color2:const Color(0xffFF93AA), txtColor: whiteColor, onPressed:(){
                      Get.toNamed('/classify');
                    },iconImage: classify, ),

                  SosCard(text:platoonTxt,color1:const Color(0xff1AB1B0), color2:const Color(0xff8FE5E5), txtColor: whiteColor, onPressed:(){
                  },iconImage: platoonImg,),

                    ],
                  ),
                ),
              ],
            ),
          )

        ],
      ),


    ),

    ),);
  }



}
