import 'dart:async';

import 'package:camera/camera.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:tflite/tflite.dart';

import '../util/available_cameras.dart';
import '../util/color_constants.dart';
import '../util/string_constants.dart';
import '../widget/app_bar.dart';
import '../widget/cupertino_switch.dart';
import '../widget/text_widget.dart';

class ImageClassifyScreen extends StatefulWidget {
  const ImageClassifyScreen({Key? key}) : super(key: key);

  @override
  State<ImageClassifyScreen> createState() => _ImageClassifyScreenState();
}

class _ImageClassifyScreenState extends State<ImageClassifyScreen> {
  CameraController? controller;
  CameraImage? cameraImage;
  bool isCamera = false, isDetect = false, isMotor = false;
  ResolutionPreset currentResolutionPreset = ResolutionPreset.max;
  bool _isCameraInitialized = false;
  TextEditingController valueController = TextEditingController();
  TextEditingController predictController = TextEditingController();
  FlashMode? _currentFlashMode;
  PlatformFile? _selectModel;
  List? recognitionsList = [];
  StreamController<List<dynamic>> recognitionController = StreamController();
  Stream get recognitionStream => recognitionController.stream;
  List<PlatformFile> models = [];
  final List<PlatformFile> staticModels = [PlatformFile(name: "mobilenet.tflite", path: "assets/mobilenet.tflite", size: 0)];


  @override
  void initState() {
    super.initState();
    getModels();
    initCamera(cameras![0]);
  }

  getModels()async{
    if(storage.getItem("Classify")!=null){
      models=storage.getItem("Classify");
      models=[...staticModels,...models];

      debugPrint("models1__${models.length}");
    }else{
      models.addAll(staticModels);

    }
  }

  Future loadModel(String? modelPath, String? textPath) async {
    try {
      // Load the TFLite model
      await Tflite.loadModel(
        model: modelPath!,
        labels: textPath!,
      ).then((value) => debugPrint("Loaded:$value"));
      debugPrint('TFLite model loaded successfully');
    } catch (e) {
      debugPrint('Failed to load TFLite model: $e');
    }
    debugPrint("models:${models.first} ${_selectModel?.name}" );

  }

  void initCamera(CameraDescription cameraDescription) async {
    // Instantiating the camera controller
    final CameraController cameraController = CameraController(
      cameraDescription,
      currentResolutionPreset,
      enableAudio: false,
      imageFormatGroup: ImageFormatGroup.bgra8888,
    );

    // Replace with the new controller
    if (mounted) {
      setState(() {
        controller = cameraController;
      });
    }

    // Update UI if controller updated
    cameraController.addListener(() {
      if (mounted) setState(() {});
    });

    // Initialize controller
    try {
      await cameraController.initialize().then((value) {
        cameraController.lockCaptureOrientation(DeviceOrientation.portraitUp);
        if (!mounted) {
          return;
        }

        setState(() {});
        _currentFlashMode = controller!.value.flashMode;

        setState(() {
          cameraController.startImageStream(
                (image) async{
              cameraImage = image;
              if(isDetect){
                runModel(cameraImage);
              }

            },
          );

          debugPrint("cameraImage$cameraImage");
        });
      });
    } on CameraException catch (e) {
      if (e is CameraException) {
        switch (e.code) {
          case 'CameraAccessDenied':
            debugPrint('User denied camera access.');
            break;
          default:
            debugPrint('Handle other errors.');
            break;
        }
      }
    }

    // Update the Boolean
    if (mounted) {
      setState(() {
        _isCameraInitialized = controller!.value.isInitialized;
      });
    }
  }

  runModel(CameraImage? cameraImage) async {
    setState(() {
      isDetect=true;
    });
    debugPrint("runModel");
    await Tflite.runModelOnFrame(
      bytesList: cameraImage!.planes.map((plane) {
        return plane.bytes;
      }).toList(), // transforms the image into a bit array
      imageHeight: cameraImage.height,
      imageWidth: cameraImage.width,
      imageMean: 127.5,
      imageStd:127.5,
      numResults: 2, threshold: 0.1,
    ).then((recognitions) {
      recognitionsList=recognitions;
      recognitions?.map((res) {
        debugPrint("recognitions:${res['label']}");

      });

      debugPrint("recognitions:$recognitions");

      // return null;
    });
    debugPrint("recognitionsList$recognitionsList");

    setState(() {
      cameraImage;
    });


/*
    if (recognitionsList!.isNotEmpty) {
      debugPrint(recognitionsList![0].toString());
      if (recognitionController.isClosed) {
        // restart if was closed
        recognitionController = StreamController();
      }
      // notify to listeners
      recognitionController.add(recognitionsList!);
    }
*/
  }

  @override
  void dispose() {
    super.dispose();
    isDetect=false;
    controller?.dispose();
    Tflite.close();

  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
        color: primaryColor,
        child: SafeArea(
            child: Scaffold(
              backgroundColor: whiteColor,
              body: SlidingUpPanel(
                defaultPanelState: PanelState.CLOSED,
                minHeight: 60,
                maxHeight: 550,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(18.0),
                    topRight: Radius.circular(18.0)),
                parallaxEnabled: true,
                panelBuilder: (scrollController) => buildSlidingPanel(
                    scrollController: scrollController, size: size),
                body: Stack(
                  children: [
                    (!controller!.value.isInitialized)
                        ? Container()
                        :Positioned(
                        top: 0.0,
                        left: 0.0,
                        width: size.width,
                        height: size.height,
                        child: SizedBox(
                            width: size.width,
                            height: size.height / controller!.value.aspectRatio,
                            child: AspectRatio(
                              aspectRatio: controller!.value.aspectRatio,
                              child: CameraPreview(controller!),
                            ))
                    ),
                    const Align(
                        alignment: Alignment.topLeft,
                        child: AppBarWidget(
                          text: classifyTxt,
                          color: whiteColor,
                        )),
                  ],
                ),
              ),
            )));
  }

  buildSlidingPanel(
      {required ScrollController scrollController, required Size size}) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(40),
        child: AppBar(
          title: const Center(
              child: Icon(
                Icons.drag_handle,
                color: darkColor,
              )),
          automaticallyImplyLeading: false,
          backgroundColor: whiteColor,
          elevation: 0,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0))),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: size.width / 2.2,
                    child: CupertinoWidget(
                      text: detectTxt,
                      isValue: isDetect,
                      onChange: (bool? val) {
                        setState(() {
                          isDetect = val!;
                        });
                        if(isDetect==false){
                          setState(() {
                            recognitionsList=[];
                            recognitionsList?.clear();
                            controller!.stopImageStream();

                            isDetect = false;
                          });
                        }else{
                          initCamera(controller!.description);
                          setState(() {
                            isDetect = true;
                          });

                        }

                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: size.width / 2.2,
                    child: CupertinoWidget(
                      text: cameraTxt,
                      isValue: isCamera,
                      onChange: (bool? val) {
                        setState(() {
                          _isCameraInitialized = false;
                        });
                        initCamera(
                          cameras![isCamera ? 0 : 1],
                        );
                        setState(() {
                          isCamera = !isCamera;
                        });
                      },
                    ),
                  ),
                )
              ]),
          Padding(
            padding: const EdgeInsets.only(top: 8, left: 32, right: 16),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: size.width / 3,
                    child: const TextWidget(
                        text: modelTxt,
                        size: 16,
                        maxLines: 2,
                        color: darkColor,
                        weight: FontWeight.w500),
                  ),
                  SizedBox(
                    width: size.width / 2,
                    child: InputDecorator(
                      decoration: const InputDecoration(border: OutlineInputBorder(borderSide: BorderSide(color: lightGreyColor),),contentPadding: EdgeInsets.only(left: 16,right: 16)),

                      child:DropdownButtonHideUnderline(
                        child: DropdownButton<PlatformFile>(
                          value: _selectModel,
                          onChanged: (value) {
                            setState(() {
                              _selectModel = value;
                              loadModel(_selectModel!.path,_selectModel?.path!.replaceAll(".tflite", ".txt"));

                            });
                          },

                          hint: const TextWidget(
                              text: "Select Model",
                              size: 13,
                              maxLines: 2,
                              align: TextAlign.start,
                              color: darkColor,
                              weight: FontWeight.w400),
                          // Hide the default underline
                          underline: Container(),
                          // set the color of the dropdown menu
                          dropdownColor: whiteColor,
                          iconEnabledColor: primaryColor,

                          isExpanded: true,

                          // The list of options
                          items: models
                              .map((e) => DropdownMenuItem<PlatformFile>(
                            value: e,
                            child: Container(
                              alignment: Alignment.centerLeft,
                              child: TextWidget(
                                  text: e.name,
                                  size: 14,
                                  maxLines: 2,
                                  align: TextAlign.start,
                                  color: primaryColor,
                                  weight: FontWeight.w500),
                            ),
                          ))
                              .toList(),
                        ),
                      ),

                    ),
                  ),
                ]),
          ),

          Padding(
            padding: const EdgeInsets.only(left: 32, right: 8),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  const TextWidget(
                      text: predictTxt,
                      size: 16,
                      color: darkColor,
                      weight: FontWeight.w500),
/*                  Padding(
                    padding: const EdgeInsets.only(right: 8.0, top: 8, left: 24),
                    child: SizedBox(
                      width: size.width / 2,
                      height: size.height / 12,
                      child: TextFormField(
                        controller: predictController,
                        textInputAction: TextInputAction.done,
                        decoration: const InputDecoration(
                            contentPadding: EdgeInsets.only(left: 8),
                            border: OutlineInputBorder(borderSide: BorderSide(color: lightGreyColor),),
                            fillColor: lightGreyColor),
                        keyboardType: TextInputType.text,
                        onSaved: (String? val) {
                          setState(() {
                            predictController.text = val!;
                            // loginOTPRequest?.username = val!;
                            // mobileNo=val;
                          });
                        },
                      ),
                    ),
                  )*/
                  _contentWidget(),
                ]),
          ),
        ],
      ),
    );
  }

/*
  Widget _contentWidget() {
    var _width = Get.width;
    var _padding = 20.0;
    var _labelWitdth = 150.0;
    var _labelConfidence = 30.0;
    var _barWitdth = _width - _labelWitdth - _labelConfidence - _padding * 2.0;

    if (recognitionsList!.isNotEmpty) {
      return Container(
        height: 150,
        child: ListView.builder(
          itemCount: recognitionsList?.length,
          itemBuilder: (context, index) {
            if (recognitionsList!.length > index) {
             var valuePer=recognitionsList![index]['confidence'].toInt();
             double percentage=valuePer!/100;
              debugPrint("label:$valuePer $percentage");

              return Row(
                children: <Widget>[
                  Container(
                      padding:
                          EdgeInsets.only(left: _padding, right: _padding),
                      width: _labelWitdth,
                      child: TextWidget(
                          text: recognitionsList![index]['label'],
                          size: 16,
                          maxLines: 1,
                          color: darkColor,
                          weight: FontWeight.w600)),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      width: _barWitdth,
                      height: 50,
                      child: LinearPercentIndicator(
                        percent: percentage,
                        animationDuration: 2000,
                        width: Get.width/2.5,
                        lineHeight: 45,
                        animation: true,
                        backgroundColor: accentColor,
                        progressColor:   primaryColor,
                        barRadius: const Radius.circular(5),
                        // backgroundColor: Colors.transparent,
                      ),
                    ),
                  ),
                  SizedBox(
                      width: 20,
                      child: TextWidget(
                          text: (recognitionsList![index]['confidence'] * 100).toStringAsFixed(0) + '%',
                          size: 16,
                          maxLines: 1,
                          color: darkColor,
                          weight: FontWeight.w500))
                ],
              );
            } else {
              return const Text('');
            }
          },
        ),
      );
    } else {
      return Text('');
    }
  }
*/

  Widget _contentWidget() {
    var _padding = 20.0;
    var _labelWitdth = 150.0;

    if (recognitionsList!=null) {
      return SizedBox(
        height: 100,
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: recognitionsList?.length,
          itemBuilder: (context, index) {
            debugPrint("recognitionsList___${(recognitionsList![index]['confidence'] * 100).toStringAsFixed(0)}%}");
            var valuePer=(recognitionsList![index]['confidence'] * 100).toStringAsFixed(0);

            // double percentage=valuePer/100;
            debugPrint("labelValue:${recognitionsList![index]['label']} ${valuePer}");
            // if (valuePer > 0.1){
            return Row(
              children: <Widget>[
                Container(
                    padding:
                    EdgeInsets.only(left: _padding, right: _padding,top: 8,bottom: 8),
                    width: _labelWitdth,
                    child: TextWidget(
                        text: recognitionsList![index]['label'],
                        size: 16,
                        maxLines: 1,
                        color: darkColor,
                        weight: FontWeight.w600)),
                Container(
                    padding:
                    EdgeInsets.only(left: _padding, right: _padding,top: 8,bottom: 8),
                    width: _labelWitdth,
                    child: TextWidget(
                        text: "$valuePer %",
                        size: 16,
                        maxLines: 1,
                        color: darkColor,
                        weight: FontWeight.w600)),
                /*   Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      width: _barWitdth,
                      height: 50,
                      child: LinearPercentIndicator(
                        percent: valuePer,
                        width: Get.width/2.5,
                        lineHeight: 45,
                        backgroundColor: accentColor,
                        progressColor:   primaryColor,
                        barRadius: const Radius.circular(5),
                        // backgroundColor: Colors.transparent,
                      ),
                    ),
                  ),*/
/*                     Container(
                       width: _barWitdth,
                       padding:
                       EdgeInsets.only(left: _padding, right: _padding,top: 8,bottom: 8),
                       child: LinearProgressIndicator(
                         backgroundColor: lightGreyColor,
                         value:valuePer.toDouble(),
                         minHeight: 40,
                         color: primaryColor,
                       ),
                     )*/
/*
                  Container(
                    width: _labelConfidence,
                    child: Text(
                      "${(recognitionsList![index]['confidence'] * 100).toStringAsFixed(0)}%",

                      // "$percentage %",
                      // (recognitionsList![index]['confidence'] * 100).toStringAsFixed(0) + '%',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontSize: 16,color: darkColor),
                    ),
                  )
*/
              ],
            );
            // }else{
            //   return const SizedBox.shrink(); // hide the item if confidence < 0.8
            // }

          },
        ),
      );
    } else {
      return const SizedBox(height: 100,
      );
    }
  }

}


