/*


import 'package:aibot_project/botConnect/bot_connect_screen.dart';
import 'package:aibot_project/data_collection/data_collection_screen.dart';
import 'package:aibot_project/object_detection/object_detection_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:wakelock/wakelock.dart';

import 'auth/splash_screen.dart';
import 'home/dashboard_screen.dart';
import 'image_classification/image_classify_screen.dart';
import 'self_drivng/self_driving_screen.dart';
import 'settings/model_management_screen.dart';

import 'settings/settings_screen.dart';
import 'util/available_cameras.dart';
import 'util/color_constants.dart';
import 'util/state/MQTTAppState.dart';
import 'util/string_constants.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  availableCamera();
  await SystemChrome.setPreferredOrientations(<DeviceOrientation>[
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown
  ]).then((_) => runApp(const MyApp()));
}



class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}
class _MyAppState extends State<MyApp>{

  @override
  Widget build(BuildContext context) {
    Wakelock.enable();

    return MultiProvider(
      providers: [
        ChangeNotifierProvider<MQTTAppState>(
          create: (_) => MQTTAppState(),
          child: const BotConnectionScreen(),
        )
      ],
      child: GetMaterialApp(
        title: appName,
        debugShowCheckedModeBanner: false,
        defaultTransition: Transition.noTransition,
        transitionDuration: const Duration(microseconds: 0),
        navigatorKey: Get.key,
        darkTheme: ThemeData.dark(),
        themeMode: ThemeMode.system,
        theme: ThemeData(
          fontFamily: 'Nunito Sans',
          primaryColor: primaryColor,
          splashColor: Colors.transparent,
          primaryColorLight: accentColor, primaryColorDark: primaryColor,
          textSelectionTheme: const TextSelectionThemeData(
            cursorColor: primaryColor,
          ),
          visualDensity: VisualDensity.adaptivePlatformDensity,
          colorScheme: ColorScheme.fromSwatch().copyWith(
            secondary: accentColor,
            primary: primaryColor,
          ),

        ),
        home: const SplashScreen(),
        // home: isViewed != 0 ? const OnBoardingScreen() : const ReqAccountScreen(),
        getPages: [
          GetPage(
            name: '/splash',
            page: () => const SplashScreen(),
          ),
          GetPage(
            name: '/dashboard',
            page: () => const DashboardScreen(),
          ) ,
          GetPage(
            name: '/setting',
            page: () => const SettingsScreen(),
          ),
          GetPage(
            name: '/connect',
            page: () => const BotConnectionScreen(),
          ),
          GetPage(
            name: '/collection',
            page: () => const DataCollectionScreen(),
          ),
          GetPage(
            name: '/detect',
            page: () => const ObjectDetectionScreen(),
          ),
          GetPage(
            name: '/classify',
            page: () => const ImageClassifyScreen(),
          ),
          GetPage(
            name: '/self',
            page: () => const SelfDrivingScreen(),
          ),
          GetPage(
            name: '/model',
            page: () => const ModelManagementScreen(),
          ),
        ],
      ),
    );
  }
}
*/


import 'dart:async';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PlatformChannel extends StatefulWidget {
  const PlatformChannel({
    super.key,
    required this.camera,
  });

  final CameraDescription camera;
  @override
  _PlatformChannelState createState() => _PlatformChannelState();
}

class _PlatformChannelState extends State<PlatformChannel> {
  late CameraController _controller;
  late Future<void> _initializeControllerFuture;

  IconData iconSwitch = Icons.battery_unknown;

  static const MethodChannel methodChannel =
  MethodChannel('beingRD.flutter.io/image');

  Future<void> _getBatteryLevel(CameraImage image) async {
    try {
      // _controller.stopImageStream();
      final String result = await methodChannel.invokeMethod('saveImage', {
        "plane": _concatenatePlanes(image.planes),
        "height": image.height,
        "width": image.width,
        // "path": "path",
        "data": "data"
      });
      print(result);
    } on PlatformException {
      print("Error while saving image");
    }
  }

  static Uint8List _concatenatePlanes(List<Plane> planes) {
    final WriteBuffer allBytes = WriteBuffer();
    for (Plane plane in planes) {
      allBytes.putUint8List(plane.bytes);
    }

    return allBytes.done().buffer.asUint8List();
  }

  @override
  void initState() {
    super.initState();

    _controller = CameraController(
      widget.camera,
      ResolutionPreset.high,
      enableAudio: false,
      imageFormatGroup: ImageFormatGroup.jpeg,
    );
    _initializeControllerFuture = _controller.initialize();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Take a picture'),
        leading: GestureDetector(
          child: Icon(Icons.camera_alt),
          onTap: () {
            _controller.stopImageStream();
          },
        ),
      ),
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return CameraPreview(_controller);
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          try {
            await _initializeControllerFuture;
            _controller.startImageStream((image) => {_getBatteryLevel(image)});
            if (!mounted) return;
          } catch (e) {
            print(e);
          }
        },
        child: const Icon(Icons.camera_alt),
      ),
    );
  }

  @override
  void dispose() {
    _controller.stopImageStream();
    _controller.dispose();
    super.dispose();
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final cameras = await availableCameras();
  final firstCamera = cameras.first;

  runApp(
    MaterialApp(
      home: PlatformChannel(
        camera: firstCamera,
      ),
    ),
  );
}

