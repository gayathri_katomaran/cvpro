
import 'package:aibot_project/widget/snack_bar.dart';
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:tflite/tflite.dart';
import 'dart:math' as math;

import '../util/available_cameras.dart';

typedef void Callback(List<dynamic> list, int h, int w);

class CameraFeed extends StatefulWidget {
   List<CameraDescription>? cameras;
   Callback? setRecognitions;
  // The cameraFeed Class takes the cameras list and the setRecognitions
  // function as argument
  bool isDetect,isCamera,isFlash;
  double? currentConfident;

  CameraFeed(this.cameras, this.setRecognitions, this.isDetect, this.isCamera, this.currentConfident, this.isFlash);

  @override
  CameraFeedState createState() => CameraFeedState();
}

class CameraFeedState extends State<CameraFeed> {
  CameraController? controller;
  bool isDetecting = false;

  @override
  void initState() {
    super.initState();
    debugPrint("widget.cameras ${widget.cameras}${widget.isCamera}");
    initCamera(widget.isCamera,widget.isFlash);

  }
  Future<void> initCamera(bool isCamera, bool isFlash) async {

    if (cameras!.isEmpty) {
      debugPrint('No Cameras Found.');
    } else {
      debugPrint("isCamera:$isCamera");

      final camera = isCamera
          ? cameras?.firstWhere(
              (camera) => camera.lensDirection == CameraLensDirection.front)
          : cameras?.firstWhere(
              (camera) => camera.lensDirection == CameraLensDirection.back);


      controller = CameraController(
        camera!,
        ResolutionPreset.high,
        imageFormatGroup: ImageFormatGroup.bgra8888
      );
      controller?.addListener(() {
        if (mounted) setState(() {});
        if (controller!.value.hasError) {
          snackBarAlert('Camera error ${controller!.value.errorDescription}');
        }      });
      controller?.initialize().then((_) {
        if (!mounted) {
          return;
        }
        setState(() {});
        controller?.startImageStream((CameraImage img) {
          if (!isDetecting && widget.isDetect) {
            isDetecting = true;
            Tflite.detectObjectOnFrame(
              bytesList: img.planes.map((plane) {return plane.bytes;}).toList(),
              model: "SSDMobileNet",
              imageHeight: img.height,
              imageWidth: img.width,
              imageMean: 127.5,
              imageStd: 127.5,
              numResultsPerClass: 1,
              threshold:widget.currentConfident!,
            ).then((recognitions) {
              /*
              When setRecognitions is called here, the parameters are being passed on to the parent widget as callback. i.e. to the LiveFeed class
               */
              widget.setRecognitions!(recognitions!, img.height, img.width);
              isDetecting = false;
            });
          }
        });
      });
      controller?.setFlashMode(
          isFlash ? FlashMode.torch : FlashMode.off);
    }
  }


  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
  @override
  void didUpdateWidget(CameraFeed oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.isCamera != oldWidget.isCamera ||
        widget.isFlash != oldWidget.isFlash) {
      Future.microtask(() async {
        if (controller != null) {
          await controller?.dispose();
        }
        await initCamera(widget.isCamera,widget.isFlash);
        setState(() {});
      });
    }
  }


  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    final CameraController? cameraController = controller;

    // App state changed before we got the chance to initialize.
    if (cameraController == null || !cameraController.value.isInitialized) {
      return;
    }

    if (state == AppLifecycleState.inactive) {
      // Free up memory when camera not active
      cameraController.dispose();
    } else if (state == AppLifecycleState.resumed) {
      // Reinitialize the camera with same properties
      initCamera(widget.isCamera,widget.isFlash);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (controller == null || !controller!.value.isInitialized) {
      return Container();
    }

    var tmp = MediaQuery.of(context).size;
    var screenH = math.max(tmp.height, tmp.width);
    var screenW = math.min(tmp.height, tmp.width);
    tmp = controller!.value.previewSize!;
    var previewH = math.max(tmp.height, tmp.width);
    var previewW = math.min(tmp.height, tmp.width);
    var screenRatio = screenH / screenW;
    var previewRatio = previewH / previewW;

    return OverflowBox(
      maxHeight:
          screenRatio > previewRatio ? screenH : screenW / previewW * previewH,
      maxWidth:
          screenRatio > previewRatio ? screenH / previewH * previewW : screenW,
      child: controller != null && controller!.value.isInitialized
          ? CameraPreview(controller!):Container(),
    );
  }
}
