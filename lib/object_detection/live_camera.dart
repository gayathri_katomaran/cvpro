import 'dart:io';

import 'package:camera/camera.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import 'package:tflite/tflite.dart';

import '../util/MQTTManager.dart';
import '../util/available_cameras.dart';
import '../util/color_constants.dart';
import '../util/state/MQTTAppState.dart';
import '../util/string_constants.dart';
import '../widget/app_bar.dart';
import '../widget/cupertino_switch.dart';
import '../widget/snack_bar.dart';
import '../widget/text_widget.dart';
import 'camera.dart';

class LiveFeed extends StatefulWidget {
  final List<CameraDescription> cameras;
  LiveFeed(this.cameras);
  @override
  _LiveFeedState createState() => _LiveFeedState();
}
class _LiveFeedState extends State<LiveFeed> {
  List<dynamic>? _recognitions;
  int _imageHeight = 0;
  int _imageWidth = 0;
  bool isCamera = false,
      isDetect = false,
      isBot = false,
      isFlash = false,
      isMotor = false,isDetecting = false;
  MQTTManager? manager;
  MQTTAppState? currentAppState;
  MqttClient? client;
  TextEditingController valueController = TextEditingController();
  double currentConfident = 0.1;
  String controlValue="";
  final GlobalKey<CameraFeedState> globalKey = GlobalKey();
  final List<PlatformFile> staticModels = [PlatformFile(name: "ssd_mobilenet.tflite", path: "assets/ssd_mobilenet.tflite", size: 0)];
  List<PlatformFile> models = [];
  PlatformFile? _selectModel;
  String? selectObject;
  List<String> objectList = [];
  List<String> filteredList = [];
  final regex = RegExp(r'[^a-zA-Z0-9]');
  String?textName,ipAdd,usrName,pass,topicValue;

  loadTfModel(String? modelPath, String? textPath) async {
    await Tflite.loadModel(
      model: modelPath!,
      labels: textPath!,
    );
  }
  /* 
  The set recognitions function assigns the values of recognitions, imageHeight and width to the variables defined here as callback
  */
  setRecognitions(recognitions, imageHeight, imageWidth) {
    setState(() {
      _recognitions = recognitions;
      _imageHeight = imageHeight;
      _imageWidth = imageWidth;
    });
  }
  @override
  void initState() { 
    super.initState();
    ipAdd = storage.getItem("IP_ADDRESS");
    usrName = storage.getItem("USER_NAME");
    pass = storage.getItem("PASSWORD");
    topicValue = storage.getItem("TOPIC");
    debugPrint("ipAdd:$ipAdd $usrName $pass");
    NetworkUtils.checkConnectivity();
    NetworkUtils.checkInternetConnection();

    NetworkUtils.startTimer();

    getModels();
  }

  getModels()async{
    final status = await Permission.camera.status;
    debugPrint("status:$status");
    if(storage.getItem("Detector")!=null){
      setState((){
        models=storage.getItem("Detector");
        models=[...staticModels,...models];
      });

      debugPrint("models1__${models.length}");
    }else{
      setState(() {
        models.addAll(staticModels);
        debugPrint("models__${models.toString()}");
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    final MQTTAppState appState = Provider.of<MQTTAppState>(context);
    // Keep a reference to the app state.
    setState((){
      currentAppState = appState;

    });
    debugPrint("appState:${currentAppState?.getAppConnectionState}");


    Size screen = MediaQuery.of(context).size;
    double factorX = screen.width;
    double factorY = screen.height;

    List<Widget> renderBox() {
      return _recognitions==null?[]:_recognitions!.map((re) {

        double midX=0.0;
        double midY=0.0;
        Offset midpoint=const Offset(0.0, 0.0);

        if( re['detectedClass']==selectObject &&isDetect==true){
          Map highestConfidenceObject = _recognitions!.reduce((a, b) => a["confidenceInClass"] > b["confidenceInClass"] ? a : b);
          debugPrint("highestConfidenceObject:${ highestConfidenceObject["rect"]["w"]}");


          double x = highestConfidenceObject["rect"]["x"];
          double y = highestConfidenceObject["rect"]["y"];
          double w = highestConfidenceObject["rect"]["w"];
          double h = highestConfidenceObject["rect"]["h"];

          Rect boundingBox = Rect.fromLTWH(x, y, w, h);
          midpoint = boundingBox.center;

          if(currentAppState?.getAppConnectionState == MQTTAppConnectionState.connected && isBot){
            if( NetworkUtils.hasInternet){
              if (midpoint.dx < 0.33) { // midpoint is on the left side of the camera
                setState(() {
                  controlValue="200,-150";
                  valueController.text=controlValue;
                  _publishMessage(controlValue);
                });

                // _botMovementMechanism.moveLeft();
              } else if (midpoint.dx > 0.66) { // midpoint is on the right side of the camera
                // _botMovementMechanism.moveRight();
                setState(() {
                  controlValue="-150,200";
                  _publishMessage(controlValue);
                  valueController.text=controlValue;

                });
              } else { // midpoint is in the center of the camera
                // _botMovementMechanism.moveStraight();
                setState(() {
                  controlValue="200,200";
                  _publishMessage(controlValue);
                  valueController.text=controlValue;

                });

              }
            }else{
              snackBarAlert("Mqtt Disconnected");
              setState(() {
                controlValue="0,0";
                valueController.text="";
                valueController.clear();
              });
            }
          }
          midX = x+(w~/2);
          midY = y+(h~/2);
          debugPrint("Midpoint of highest confident box: $boundingBox ${midpoint.dx} ${midpoint.dy}");
          debugPrint(" XYwh Value:$x $y $w $h $midX $midY");
          debugPrint(" XYwh Value:$x $y $w $h");
        }
        debugPrint("_recognitions:${re['detectedClass'].toString()}  $selectObject");

        return Stack(
          children: [
            Visibility(
              visible: re['detectedClass']==selectObject && isDetect && re['confidenceInClass'] >= currentConfident,
              // visible:  isDetect && re['confidenceInClass'] >= currentConfident,
              child: Positioned(
                left: re["rect"]["x"] * factorX,
                top: re["rect"]["y"] * factorY,
                width: re["rect"]["w"] * factorX,
                height: re["rect"]["h"] * factorY,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(10.0)),
                    border: Border.all(color: Colors.pink, width: 2.0),
                  ),
                  child: Text(
                    "${re['detectedClass']} ${(re['confidenceInClass'] * 100).toStringAsFixed(0)}%",
                    style: TextStyle(
                      background: Paint()..color = Colors.pink,
                      color: Colors.black,
                      fontSize: 18.0,
                    ),
                  ),
                ),
              ),
            ),
          ],
        );
      }).toList();
    }

    return Container(
      color: primaryColor,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: whiteColor,

          body: SlidingUpPanel(
            defaultPanelState: PanelState.CLOSED,
            minHeight: 50,
            maxHeight: 610,
            borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(18.0),
                topRight: Radius.circular(18.0)),
            parallaxEnabled: true,
            panelBuilder: (scrollController) => buildSlidingPanel(
                scrollController: scrollController, size: MediaQuery.of(context).size),
            body: Stack(
              children: <Widget>[

                CameraFeed(widget.cameras, setRecognitions,isDetect,isCamera,currentConfident,isFlash),
                Visibility(
                  visible:isDetect,
                  child: Stack(children: renderBox(),)
                ),
                const Align(
                    alignment: Alignment.topLeft,
                    child: AppBarWidget(
                      text: objectTxt,
                      color: whiteColor,
                    )),
              ],
            ),
          ),


        ),
      ),
    );
  }
  Future<void> toggleIsData() async {
    if (isBot==true) {
      _configureAndConnect();
    } else {
      setState(() {
        controlValue="0,0";
        valueController.text="";
        valueController.clear();
      });
      _publishMessage(controlValue);
      client?.unsubscribe(topicValue!);

    }
  }
  void _publishMessage(String text) {
    debugPrint("ControlValue:$text");
    String osPrefix = 'Flutter_iOS';
    if (Platform.isAndroid) {
      osPrefix = 'Flutter_Android';
    }
    manager?.publish(text,topicValue);
  }
  _configureAndConnect() {
    debugPrint("IP__$ipAdd  $usrName $pass");
    debugPrint("Calling");
    String osPrefix = 'Flutter_iOS';
    manager = MQTTManager(
      host:ipAdd!,
      topic:topicValue!,
      identifier: osPrefix, state: currentAppState!,
    );
    manager?.initializeMQTTClient();
    manager?.connect(userName,password);
  }

  buildSlidingPanel(
      {required ScrollController scrollController, required Size size}) {
    debugPrint("ModelBuild:${models.toString()}");
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(40),
        child: AppBar(
          title: const Center(
              child: Icon(
                Icons.drag_handle,
                color: darkColor,
              )),
          automaticallyImplyLeading: false,
          backgroundColor: whiteColor,
          elevation: 0,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0))),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: size.width / 2.2,
                    child: CupertinoWidget(
                      text: detectTxt,
                      isValue: isDetect,
                      onChange: (bool? val) {
                        setState(() {
                          isDetect = val!;
                        });
                        if(isDetect==false){
                          setState(() {
                            _recognitions=[];
                            _recognitions?.clear();
                            isDetect = false;

                          });

                        }else{
                          // runModel();
                        }

                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: size.width / 2.2,
                    child: CupertinoWidget(
                      text: botTxt,
                      isValue: isBot,
                      onChange: currentAppState?.getAppConnectionState == MQTTAppConnectionState.connected?(bool? val) {
                        setState(() {
                          isBot = val!;
                        });
                        toggleIsData();
                      }:null,
                    ),
                  ),
                )
              ]),
          Column(
            children: [

              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SizedBox(
                        width: size.width / 2.2,
                        child: CupertinoWidget(
                          text: cameraTxt,
                          isValue: isCamera,
                          onChange: (bool? val) {
                            setState(() {
                              isCamera=val!;
                            });
                                  setState(() {
                                      CameraFeedState().initCamera(isCamera,isFlash);
                                  });
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SizedBox(
                        width: size.width / 2.2,
                        child: CupertinoWidget(
                          text: flashTxt,
                          isValue: isFlash,
                          onChange: (bool? val) async{
                            setState(() {
                              isFlash = val!;
                            });
                            CameraFeedState().initCamera(isCamera,isFlash);

                          },
                        ),
                      ),
                    )
                  ]),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only( top: 8,left: 32,right: 16),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,

                children: [
                  SizedBox(
                    width: size.width / 3,
                    child: const TextWidget(
                        text: modelTxt,
                        size: 16,
                        maxLines: 2,
                        color: darkColor,
                        weight: FontWeight.w500),
                  ),

                  SizedBox(
                    width: size.width / 2,
                    child: InputDecorator(
                      decoration: const InputDecoration(border: OutlineInputBorder(borderSide: BorderSide(color: lightGreyColor),),contentPadding: EdgeInsets.only(left: 16,right: 16)),

                      child:DropdownButtonHideUnderline(
                        child: DropdownButton<PlatformFile>(
                          value: _selectModel,
                          onChanged: (value) async{
                            setState(() {
                              _selectModel = value;
                              debugPrint("_selectModel:${_selectModel?.path}");
                            });
                            if(_selectModel!.path!.isNotEmpty){
                              textName=_selectModel?.path!.replaceAll(".tflite", ".txt");
                              debugPrint("TextName:$textName");
                              try {
                                final file =  File(textName!);
                                final fileExists = await file.exists();


                                debugPrint("Exists:$fileExists");
                           /*     String fileContent = await file.readAsString();
                                debugPrint("fileContent:$fileContent");*/

                                String loadedString = await rootBundle.loadString(textName!);
                                setState(() {
                                  objectList = loadedString.split('\n');
                                  filteredList = objectList.where((item) => !regex.hasMatch(item)).toList();
                                });
                                debugPrint("loadedString: $loadedString $objectList");
                              } catch (e) {
                                // If encountering an error, return 0.
                                debugPrint("exception__$e");
                              }


                            }
                          },
                          hint: const TextWidget(
                              text: "Select Model",
                              size: 13,
                              maxLines: 2,
                              align: TextAlign.start,
                              color: darkColor,
                              weight: FontWeight.w400),
                          // Hide the default underline
                          underline: Container(),
                          // set the color of the dropdown menu
                          dropdownColor: whiteColor,
                          iconEnabledColor: primaryColor,

                          isExpanded: true,

                          // The list of options
                          items: models
                              .map((e) => DropdownMenuItem<PlatformFile>(
                            value: e,
                            child: Container(
                              alignment: Alignment.centerLeft,
                              child: TextWidget(
                                  text: e.name,
                                  size: 14,
                                  maxLines: 2,
                                  align: TextAlign.start,
                                  color: primaryColor,
                                  weight: FontWeight.w500),
                            ),
                          ))
                              .toList(),
                        ),
                      ),

                    ),
                  ),
                ]),
          ),
          Padding(
            padding: const EdgeInsets.only( top: 8,left: 32,right: 16),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,

                children: [
                  SizedBox(
                    width: size.width / 3,
                    child: const TextWidget(
                        text: objectText,
                        size: 16,
                        maxLines: 2,
                        color: darkColor,
                        weight: FontWeight.w500),
                  ),
                  SizedBox(
                    width: size.width / 2,
                    child: InputDecorator(
                      decoration: const InputDecoration(border: OutlineInputBorder(borderSide: BorderSide(color: lightGreyColor),),contentPadding: EdgeInsets.only(left: 16,right: 16)),

                      child:DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                          value: selectObject,
                          onChanged: (value) {
                            setState(() {
                              selectObject = value;
                              loadTfModel(_selectModel?.path,_selectModel?.path!.replaceAll(".tflite", ".txt"));

                              debugPrint("Load_Path:${_selectModel!.path}  ${_selectModel?.path!.replaceAll(".tflite", ".txt")}");
                            });
                          },
                          hint: const TextWidget(
                              text: "Select Object",
                              size: 13,
                              maxLines: 2,
                              align: TextAlign.start,
                              color: darkColor,
                              weight: FontWeight.w400),
                          // Hide the default underline
                          underline: Container(),
                          // set the color of the dropdown menu
                          dropdownColor: whiteColor,
                          iconEnabledColor: primaryColor,
                          isExpanded: true,
                          // The list of options
                          items: filteredList
                              .map((e) => DropdownMenuItem(
                            value: e,
                            child: Container(
                              alignment: Alignment.centerLeft,
                              child: TextWidget(
                                  text: e,
                                  size: 14,
                                  maxLines: 2,
                                  align: TextAlign.start,
                                  color: primaryColor,
                                  weight: FontWeight.w500),
                            ),
                          ))
                              .toList(),
                        ),
                      ),

                    ),
                  ),
                ]),
          ),
          Padding(
            padding: const EdgeInsets.only( top: 8,left: 32,right: 16),
            child: Row(
              children: [
                SizedBox(
                  width: size.width / 3.1,
                  child: const TextWidget(
                      text: confidentTxt,
                      size: 16,
                      maxLines: 2,
                      color: darkColor,
                      weight: FontWeight.w500),
                ),
                SizedBox(
                  width: size.width/2.6,
                  child:  Slider(
                    value: currentConfident,
                    min: 0,
                    max: 1,
                    onChanged: (value) {
                      setState(() {
                        currentConfident = value;
                      });
                    },
                  ),),
                Card(
                  elevation: 4,
                  child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child:TextWidget(
                          text: currentConfident.toStringAsFixed(1),
                          size: 16,
                          maxLines: 2,
                          align: TextAlign.start,
                          color: primaryColor,
                          weight: FontWeight.w500)
                  ),
                ),
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.only( left: 32,right: 8,top: 24),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const TextWidget(
                      text: controlTxt,
                      size: 16,
                      color: darkColor,
                      weight: FontWeight.w500),
              Padding(
                padding: const EdgeInsets.only(right: 8.0,  left: 24),
                child: SizedBox(
                  width: size.width / 2,
                  child: TextWidget(
                      text: valueController.text,
                      size: 16,
                      color: primaryColor,
                      weight: FontWeight.w500),
                ),
              ),
                ]),
          ),
        ],
      ),
    );
  }
}