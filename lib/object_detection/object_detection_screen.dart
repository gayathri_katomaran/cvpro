import 'dart:io';

import 'dart:async';
import 'package:aibot_project/widget/snack_bar.dart';
import 'package:camera/camera.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:tflite/tflite.dart';

import '../util/MQTTManager.dart';
import '../util/available_cameras.dart';
import '../util/color_constants.dart';
import '../util/state/MQTTAppState.dart';
import '../util/string_constants.dart';
import '../widget/app_bar.dart';
import '../widget/cupertino_switch.dart';
import '../widget/text_widget.dart';

class ObjectDetectionScreen extends StatefulWidget {
  const ObjectDetectionScreen({Key? key}) : super(key: key);

  @override
  State<ObjectDetectionScreen> createState() => _ObjectDetectionScreenState();
}

class _ObjectDetectionScreenState extends State<ObjectDetectionScreen> {
  CameraController? controller;
  CameraImage? cameraImage;
  bool isCamera = false,
      isDetect = false,
      isBot = false,
      isFlash = false,
      isMotor = false;

  ResolutionPreset currentResolutionPreset = ResolutionPreset.max;
  bool _isCameraInitialized = false;
  TextEditingController valueController = TextEditingController();
  double minConfident = 10;
  double maxConfident = 100;
  double currentConfident = 0.1;
  List? recognitionsList=[];
  String? selectObject;
  FlashMode? _currentFlashMode;
  List<PlatformFile> models = [];
  PlatformFile? _selectModel;
  List<String> objectList = [];
  List<String> filteredList = [];
  final regex = RegExp(r'[^a-zA-Z0-9]');
  String controlValue="";
  MQTTManager? manager;
  MQTTAppState? currentAppState;
  String?textName,ipAdd,usrName,pass,topicValue;
  // final List<PlatformFile> staticModels = [PlatformFile(name: "yolov2_tiny.tflite", path: "assets/yolov2_tiny.tflite", size: 0)];
  MqttClient? client;
  final List<PlatformFile> staticModels = [PlatformFile(name: "ssd_mobilenet.tflite", path: "assets/ssd_mobilenet.tflite", size: 0)];

  @override
  void initState() {
    super.initState();
    ipAdd = storage.getItem("IP_ADDRESS");
    usrName = storage.getItem("USER_NAME");
    pass = storage.getItem("PASSWORD");
    topicValue = storage.getItem("TOPIC");
    debugPrint("ipAdd:$ipAdd $usrName $pass");
    NetworkUtils.checkConnectivity();
    NetworkUtils.checkInternetConnection();

    NetworkUtils.startTimer();

    getModels();
    initCamera(cameras![0]);
  }
  getModels()async{
    final status = await Permission.camera.status;
    debugPrint("status:$status");
    if(storage.getItem("Detector")!=null){
      models=storage.getItem("Detector");
      models=[...staticModels,...models];

      debugPrint("models1__${models.length}");
    }else{
      models.addAll(staticModels);

    }
  }

  void initCamera(CameraDescription cameraDescription) async {

    // Instantiating the camera controller
    final CameraController cameraController = CameraController(
      cameraDescription,
      currentResolutionPreset,
      enableAudio: false,
      imageFormatGroup: ImageFormatGroup.bgra8888,
    );
    cameraController.lockCaptureOrientation(DeviceOrientation.portraitUp);

    // Replace with the new controller
    if (mounted) {
      setState(() {
        controller = cameraController;
      });
    }

    // Update UI if controller updated
    cameraController.addListener(() {
      if (mounted) setState(() {});
    });

    // Initialize controller
    try {
      await cameraController.initialize().then((value) {
        cameraController.lockCaptureOrientation(DeviceOrientation.portraitUp);
        if (!mounted) {
          return;
        }

        setState(() {});
        _currentFlashMode = controller!.value.flashMode;
        setState(() {
          cameraController.startImageStream(
                (image) {
              cameraImage = image;
              if(isDetect){
                runModel();
              }
            },
          );

          debugPrint("cameraImage$cameraImage");
        });
      });
    } on CameraException catch (e) {
      if (e is CameraException) {
        switch (e.code) {
          case 'CameraAccessDenied':
            debugPrint('User denied camera access.');
            break;
          default:
            debugPrint('Handle other errors.');
            break;
        }
      }
    }

    // Update the Boolean
    if (mounted) {
      setState(() {
        _isCameraInitialized = controller!.value.isInitialized;
      });
    }
  }
  runModel() async {
    setState(() {
      isDetect=true;
    });

    debugPrint("ImageSize:${cameraImage!.height} ${ cameraImage!.width}");
    await Tflite.detectObjectOnFrame(
      bytesList: cameraImage!.planes.map((plane) {
        return plane.bytes;
      }).toList(),
      model:"SSDMobileNet",
      imageHeight: cameraImage!.height,
      imageWidth: cameraImage!.width,
      imageMean: 127.5,
      imageStd: 127.5,
      numResultsPerClass: 1,
      threshold: currentConfident,
    ).then((recognitions) {
      recognitionsList = recognitions;
      recognitions?.map((res) {
        debugPrint("recognitions:${res['label']}");
      });
    });
    debugPrint("recognitionsList$recognitionsList");

    setState(() {
      cameraImage;
    });
  }



  Future loadModel(String? modelPath, String? textPath) async {
    try {
      // Load the TFLite model
      await Tflite.loadModel(
        model: modelPath!,
        labels: textPath!,
      ).then((value) => debugPrint("Loaded:$value"));
      debugPrint('TFLite model loaded successfully');
    } catch (e) {
      debugPrint('Failed to load TFLite model: $e');
    }
    debugPrint("models:${models.first} ${_selectModel?.name}" );

  }

  @override
  void dispose() {
    super.dispose();
    // controller?.dispose();
    isDetect=false;
    controller?.dispose();
    Tflite.close();
    NetworkUtils.stopTimer();

  }

  @override
  Widget build(BuildContext context) {
    final MQTTAppState appState = Provider.of<MQTTAppState>(context);
    // Keep a reference to the app state.
    currentAppState = appState;
    debugPrint("appState:${currentAppState?.getAppConnectionState}");

    //
    // if(currentAppState?.getAppConnectionState == MQTTAppConnectionState.connected){
    //   snackBarAlert("Mqtt Connected");
    // }else{
    //   snackBarAlert("Mqtt Disconnected, Please check tour internet connection");
    // }

    Size size = MediaQuery.of(context).size;
    List<Widget> list = [];
    list.add(
      Stack(
        children: [
          (!controller!.value.isInitialized)
          ? Container()
          :Positioned(
          top: 0.0,
          left: 0.0,
          width: size.width,
          height: size.height,
          child: SizedBox(
              width: size.width,
              height: size.height / controller!.value.aspectRatio,
              child: AspectRatio(
                aspectRatio: controller!.value.aspectRatio,
                child: CameraPreview(controller!),
              ))
      ),
          const Align(
              alignment: Alignment.topLeft,
              child: AppBarWidget(
                text: objectTxt,
                color: whiteColor,
              )),
/*
          Positioned(
            bottom: 150,
            child: Row(
              children: [
                SizedBox(
                  width: size.width / 3.3,
                  child: RadioListTile(
                    activeColor: primaryColor,
                    contentPadding: EdgeInsets.zero,
                    title: const TextWidget(
                        text: wideTxt,
                        size: 16,
                        color: darkColor,
                        weight: FontWeight.w500),
                    value: minAvailableZoom,
                    groupValue: currentZoomLevel,
                    onChanged: (value) async {
                      setState(() {
                        currentZoomLevel = value!;
                      });
                      await controller!.setZoomLevel(value!);
                    },
                  ),
                ),
                SizedBox(
                  width: size.width / 3,
                  child: RadioListTile(
                    contentPadding: EdgeInsets.zero,
                    title: const TextWidget(
                        text: primaryTxt,
                        size: 16,
                        color: darkColor,
                        weight: FontWeight.w500),
                    activeColor: primaryColor,
                    value: 5.0,
                    groupValue: currentZoomLevel,
                    onChanged: (value) async {
                      setState(() {
                        currentZoomLevel = value!;
                      });
                      await controller?.setZoomLevel(value!);
                    },
                  ),
                ),
                SizedBox(
                  width: size.width / 3,
                  child: RadioListTile(
                    contentPadding: EdgeInsets.zero,
                    title: const TextWidget(
                        text: zoomTxt,
                        size: 16,
                        color: darkColor,
                        weight: FontWeight.w500),
                    activeColor: primaryColor,
                    value: maxAvailableZoom,
                    groupValue: currentZoomLevel,
                    onChanged: (value) async {
                      setState(() {
                        currentZoomLevel = value!;
                      });
                      await controller!.setZoomLevel(value!);
                    },
                  ),
                )
                */
/* SizedBox(
                        width: size.width/1.2,
                        child: Slider(
                          value: currentZoomLevel,
                          min: minAvailableZoom,
                          max: maxAvailableZoom,
                          activeColor: Colors.white,
                          inactiveColor: Colors.white30,
                          onChanged: (value) async {
                            setState(() {
                              currentZoomLevel = value;
                            });
                            await controller!.setZoomLevel(value);
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.black87,
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            '${currentZoomLevel.toStringAsFixed(1)}x',
                            style: const TextStyle(color: Colors.white),
                          ),
                        ),
                      ),*//*

              ],
            ),
          )
*/
        ],
      ),

    );

    if (cameraImage != null) {
      list.addAll(displayBoxesAroundRecognizedObjects(size));
      debugPrint("list:${list.first}");
    }

    return SafeArea(
      child: Scaffold(
        backgroundColor: whiteColor,
        body: SlidingUpPanel(
          defaultPanelState: PanelState.CLOSED,
          minHeight: 50,
          maxHeight: 610,
          borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(18.0),
              topRight: Radius.circular(18.0)),
          parallaxEnabled: true,
          panelBuilder: (scrollController) => buildSlidingPanel(
              scrollController: scrollController, size: size),
          body: Stack(
              children: list
          ),
        ),
      ),
    );

  }

  List<Widget> displayBoxesAroundRecognizedObjects(Size screen) {
    if (recognitionsList == null) return [];
    double factorX = screen.width;
    double factorY = screen.height;

    Color colorPick = Colors.pink;


    return recognitionsList!.map((result) {
      double midX=0.0;
      double midY=0.0;
      Offset midpoint=const Offset(0.0, 0.0);

      if( result['detectedClass']==selectObject &&isDetect==true){
        Map highestConfidenceObject = recognitionsList!.reduce((a, b) => a["confidenceInClass"] > b["confidenceInClass"] ? a : b);
        debugPrint("highestConfidenceObject:${ highestConfidenceObject["rect"]["w"]}");


        double x = highestConfidenceObject["rect"]["x"];
        double y = highestConfidenceObject["rect"]["y"];
        double w = highestConfidenceObject["rect"]["w"];
        double h = highestConfidenceObject["rect"]["h"];

        Rect boundingBox = Rect.fromLTWH(x, y, w, h);
        midpoint = boundingBox.center;

        if(currentAppState!.getAppConnectionState == MQTTAppConnectionState.connected && isBot){
          if( NetworkUtils.hasInternet){
            if (midpoint.dx < 0.33) { // midpoint is on the left side of the camera
              setState(() {
                controlValue="200,-150";
                valueController.text=controlValue;
                _publishMessage(controlValue);
              });

              // _botMovementMechanism.moveLeft();
            } else if (midpoint.dx > 0.66) { // midpoint is on the right side of the camera
              // _botMovementMechanism.moveRight();
              setState(() {
                controlValue="-150,200";
                _publishMessage(controlValue);
                valueController.text=controlValue;

              });
            } else { // midpoint is in the center of the camera
              // _botMovementMechanism.moveStraight();
              setState(() {
                controlValue="200,200";
                _publishMessage(controlValue);
                valueController.text=controlValue;

              });

            }
          }else{
            snackBarAlert("Mqtt Disconnected");
            setState(() {
              controlValue="0,0";
              valueController.text="";
              valueController.clear();
            });
          }
        }
        midX = x+(w~/2);
        midY = y+(h~/2);
        debugPrint("Midpoint of highest confident box: $boundingBox ${midpoint.dx} ${midpoint.dy}");
        debugPrint(" XYwh Value:$x $y $w $h $midX $midY");
        debugPrint(" XYwh Value:$x $y $w $h");
        debugPrint("result:${result['detectedClass']}  ${currentConfident.toStringAsFixed(0)}");

      }

      return  Stack(
        children: [

          Visibility(
            visible: result['detectedClass']==selectObject && isDetect && result['confidenceInClass'] >= currentConfident,
            child: Positioned(
              left: result["rect"]["x"] * factorX,
              top: result["rect"]["y"] * factorY,
              width: result["rect"]["w"] * factorX,
              height: result["rect"]["h"] * factorY,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(10.0)),
                  border: Border.all(color: Colors.pink, width: 2.0),
                ),
                child: Text(
                  "${result['detectedClass']} ${(result['confidenceInClass'] * 100).toStringAsFixed(0)}%",
                  style: TextStyle(
                    background: Paint()..color = colorPick,
                    color: Colors.black,
                    fontSize: 18.0,
                  ),
                ),
              ),
            ),
          ),
        ],
      );
    }).toList();
  }

  void _publishMessage(String text) {
    debugPrint("ControlValue:$text");
    String osPrefix = 'Flutter_iOS';
    if (Platform.isAndroid) {
      osPrefix = 'Flutter_Android';
    }
    manager?.publish(text,topicValue);
  }

  _configureAndConnect() {
    debugPrint("IP__$ipAdd  $usrName $pass");
    debugPrint("Calling");
    String osPrefix = 'Flutter_iOS';
    manager = MQTTManager(
      host:ipAdd!,
      topic:topicValue!,
      identifier: osPrefix, state: currentAppState!,
    );
    manager?.initializeMQTTClient();
    manager?.connect(userName,password);
  }
  Future<void> toggleIsData() async {
    if (isBot==true) {
      _configureAndConnect();
    } else {
      setState(() {
        controlValue="0,0";
        valueController.text="";
        valueController.clear();
      });
      _publishMessage(controlValue);
      client?.unsubscribe(topicValue!);

    }
  }


  buildSlidingPanel(
      {required ScrollController scrollController, required Size size}) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(40),
        child: AppBar(
          title: const Center(
              child: Icon(
                Icons.drag_handle,
                color: darkColor,
              )),
          automaticallyImplyLeading: false,
          backgroundColor: whiteColor,
          elevation: 0,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0))),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: size.width / 2.2,
                    child: CupertinoWidget(
                      text: detectTxt,
                      isValue: isDetect,
                      onChange: (bool? val) {
                        setState(() {
                          isDetect = val!;
                        });
                        if(isDetect==false){
                          setState(() {
                            recognitionsList=[];
                            recognitionsList?.clear();
                            isDetect = false;

                          });

                        }else{
                          runModel();
                        }

                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: size.width / 2.2,
                    child: CupertinoWidget(
                      text: botTxt,
                      isValue: isBot,
                      onChange: currentAppState?.getAppConnectionState == MQTTAppConnectionState.connected?(bool? val) {
                        setState(() {
                          isBot = val!;
                        });
                        toggleIsData();
                      }:null,
                    ),
                  ),
                )
              ]),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: size.width / 2.2,
                    child: CupertinoWidget(
                      text: cameraTxt,
                      isValue: isCamera,
                      onChange: (bool? val) {
                        setState(() {
                          _isCameraInitialized = false;
                        });
                        initCamera(
                          cameras![isCamera ? 0 : 1],
                        );
                        setState(() {
                          isCamera = !isCamera;
                        });
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: size.width / 2.2,
                    child: CupertinoWidget(
                      text: flashTxt,
                      isValue: isFlash,
                      onChange: (bool? val) async{
                        setState(() {
                          isFlash = val!;
                        });
                        if(isFlash){
                          setState(() {
                            _currentFlashMode = FlashMode.torch;

                          });
                          await controller?.setFlashMode(
                            FlashMode.torch,
                          );
                        }else{
                          setState(() {
                            _currentFlashMode = FlashMode.off;

                          });
                          await controller?.setFlashMode(
                            FlashMode.off,
                          );
                        }

                      },
                    ),
                  ),
                )
              ]),
          Padding(
            padding: const EdgeInsets.only( top: 8,left: 32,right: 16),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,

                children: [
                  SizedBox(
                    width: size.width / 3,
                    child: const TextWidget(
                        text: modelTxt,
                        size: 16,
                        maxLines: 2,
                        color: darkColor,
                        weight: FontWeight.w500),
                  ),

                  SizedBox(
                    width: size.width / 2,
                    child: InputDecorator(
                      decoration: const InputDecoration(border: OutlineInputBorder(borderSide: BorderSide(color: lightGreyColor),),contentPadding: EdgeInsets.only(left: 16,right: 16)),

                      child:DropdownButtonHideUnderline(
                        child: DropdownButton<PlatformFile>(
                          value: _selectModel,
                          onChanged: (value) async{
                            setState(() {
                              _selectModel = value;
                              debugPrint("_selectModel:${_selectModel?.path}");
                            });
                            if(_selectModel!.path!.isNotEmpty){
                              textName=_selectModel?.path!.replaceAll(".tflite", ".txt");
                              debugPrint("TextName:$textName");
                              try {
                                final file =  File(textName!);
                                final fileExists = await file.exists();


                                debugPrint("Exists:$fileExists");

                                String loadedString = await rootBundle.loadString(textName!);
                                setState(() {
                                  objectList = loadedString.split('\n');
                                  filteredList = objectList.where((item) => !regex.hasMatch(item)).toList();
                                });
                                debugPrint("loadedString: $loadedString $objectList");
                              } catch (e) {
                                // If encountering an error, return 0.
                                debugPrint("exception__$e");
                              }


                            }
                          },
                          hint: const TextWidget(
                              text: "Select Model",
                              size: 13,
                              maxLines: 2,
                              align: TextAlign.start,
                              color: darkColor,
                              weight: FontWeight.w400),
                          // Hide the default underline
                          underline: Container(),
                          // set the color of the dropdown menu
                          dropdownColor: whiteColor,
                          iconEnabledColor: primaryColor,

                          isExpanded: true,

                          // The list of options
                          items: models
                              .map((e) => DropdownMenuItem<PlatformFile>(
                            value: e,
                            child: Container(
                              alignment: Alignment.centerLeft,
                              child: TextWidget(
                                  text: e.name,
                                  size: 14,
                                  maxLines: 2,
                                  align: TextAlign.start,
                                  color: primaryColor,
                                  weight: FontWeight.w500),
                            ),
                          ))
                              .toList(),
                        ),
                      ),

                    ),
                  ),
                ]),
          ),
          Padding(
            padding: const EdgeInsets.only( top: 8,left: 32,right: 16),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,

                children: [
                  SizedBox(
                    width: size.width / 3,
                    child: const TextWidget(
                        text: objectText,
                        size: 16,
                        maxLines: 2,
                        color: darkColor,
                        weight: FontWeight.w500),
                  ),
                  SizedBox(
                    width: size.width / 2,
                    child: InputDecorator(
                      decoration: const InputDecoration(border: OutlineInputBorder(borderSide: BorderSide(color: lightGreyColor),),contentPadding: EdgeInsets.only(left: 16,right: 16)),

                      child:DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                          value: selectObject,
                          onChanged: (value) {
                            setState(() {
                              selectObject = value;
                              loadModel(_selectModel?.path,_selectModel?.path!.replaceAll(".tflite", ".txt"));

                              debugPrint("Load_Path:${_selectModel!.path}  ${_selectModel?.path!.replaceAll(".tflite", ".txt")}");
                            });
                          },
                          hint: const TextWidget(
                              text: "Select Object",
                              size: 13,
                              maxLines: 2,
                              align: TextAlign.start,
                              color: darkColor,
                              weight: FontWeight.w400),
                          // Hide the default underline
                          underline: Container(),
                          // set the color of the dropdown menu
                          dropdownColor: whiteColor,
                          iconEnabledColor: primaryColor,
                          isExpanded: true,
                          // The list of options
                          items: filteredList
                              .map((e) => DropdownMenuItem(
                            value: e,
                            child: Container(
                              alignment: Alignment.centerLeft,
                              child: TextWidget(
                                  text: e,
                                  size: 14,
                                  maxLines: 2,
                                  align: TextAlign.start,
                                  color: primaryColor,
                                  weight: FontWeight.w500),
                            ),
                          ))
                              .toList(),
                        ),
                      ),

                    ),
                  ),
                ]),
          ),
          Padding(
            padding: const EdgeInsets.only( top: 8,left: 32,right: 16),
            child: Row(
              children: [
                SizedBox(
                  width: size.width / 3.1,
                  child: const TextWidget(
                      text: confidentTxt,
                      size: 16,
                      maxLines: 2,
                      color: darkColor,
                      weight: FontWeight.w500),
                ),
              SizedBox(
                width: size.width/2.3,
                child:  Slider(
                  value: currentConfident,
                  min: 0,
                  max: 1,
                  onChanged: (value) {
                    setState(() {
                      currentConfident = value;
                    });
                  },
                ),),
                Card(
                  elevation: 4,
                  child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child:TextWidget(
                          text: currentConfident.toStringAsFixed(1),
                          size: 16,
                          maxLines: 2,
                          align: TextAlign.start,
                          color: primaryColor,
                          weight: FontWeight.w500)
                  ),
                ),
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.only( left: 32,right: 8),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const TextWidget(
                      text: controlTxt,
                      size: 16,
                      color: darkColor,
                      weight: FontWeight.w500),

                   Padding(
                     padding: const EdgeInsets.only(right: 8.0, top: 16, left: 24),
                     child: SizedBox(
                       width: size.width / 2,
                       height: size.height / 12,
                       child: TextWidget(
                          text: valueController.text,
                          size: 16,
                          color: darkColor,
                          weight: FontWeight.w500),
                     ),
                   ),
/*                  Padding(
                    padding: const EdgeInsets.only(right: 8.0, top: 16, left: 24),
                    child: SizedBox(
                      width: size.width / 2,
                      height: size.height / 12,
                      child: TextFormField(
                        controller: valueController,
                        textInputAction: TextInputAction.done,
                        decoration: const InputDecoration(
                            contentPadding: EdgeInsets.only(left: 8),
                            border: OutlineInputBorder(borderSide: BorderSide(color: lightGreyColor),),
                            fillColor: lightGreyColor),
                        keyboardType: TextInputType.text,
                        onSaved: (String? val) {
                          setState(() {
                            valueController.text = val!;
                            // loginOTPRequest?.username = val!;
                            // mobileNo=val;
                          });
                        },
                      ),
                    ),
                  )*/
                ]),
          ),
/*
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: SizedBox(
              width: size.width / 1.8,
              child: CupertinoWidget(
                text: motorTxt,
                isValue: isMotor,
                onChange: (bool? val) {
                  setState(() {
                    isMotor = val!;
                  });
                },
              ),
            ),
          )
*/
        ],
      ),
    );
  }
}
