import 'dart:async';

import 'package:camera/camera.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:mqtt_client/mqtt_server_client.dart';
import 'package:provider/provider.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:tflite/tflite.dart';

import '../util/MQTTManager.dart';
import '../util/available_cameras.dart';
import '../util/color_constants.dart';
import '../util/state/MQTTAppState.dart';
import '../util/string_constants.dart';
import '../widget/app_bar.dart';
import '../widget/cupertino_switch.dart';
import '../widget/snack_bar.dart';
import '../widget/text_widget.dart';

class SelfDrivingScreen extends StatefulWidget {
  const SelfDrivingScreen({Key? key}) : super(key: key);

  @override
  State<SelfDrivingScreen> createState() => _SelfDrivingScreenState();
}

class _SelfDrivingScreenState extends State<SelfDrivingScreen> {
  CameraController? controller;
  CameraImage? cameraImage;
  bool isCamera = false,
      isAuto = false,
      isBot = false,
      isFlash = false,
      isMotor = false;
  StreamSubscription<ConnectivityResult>? subscription;
  final resolutionPresets = ResolutionPreset.values;
  ResolutionPreset currentResolutionPreset = ResolutionPreset.low;
  bool _isCameraInitialized = false;
  TextEditingController valueController = TextEditingController();
  TextEditingController predictController = TextEditingController();
  FlashMode? _currentFlashMode;
  PlatformFile? _selectModel;
  List? recognitionsList=[];
  StreamController<List<dynamic>> recognitionController = StreamController();
  Stream get recognitionStream => recognitionController.stream;
  List<PlatformFile> models = [];
  MQTTManager? manager;
  MQTTAppState? currentAppState;
  String controlValue="",predictValue="";
  final List<PlatformFile> staticModels = [PlatformFile(name: "selfdriving.tflite", path: "assets/selfdriving.tflite", size: 0)];
  String?ipAdd,usrName,pass,topicValue;
  MqttServerClient? _client;
  @override
  void initState() {
    super.initState();
    ipAdd = storage.getItem("IP_ADDRESS");
    usrName = storage.getItem("USER_NAME");
    pass = storage.getItem("PASSWORD");
    topicValue = storage.getItem("TOPIC");
    getModels();

    initCamera(cameras![0]);
    NetworkUtils.checkConnectivity();
    NetworkUtils.checkInternetConnection();
    NetworkUtils.startTimer();
  }

  getModels()async{
    if(storage.getItem("Driving")!=null){
      setState((){
        models=storage.getItem("Driving");
        models=[...staticModels,...models];
      });
      debugPrint("models1__${models.length}");
    }else{
      models.addAll(staticModels);

    }
  }
  Future loadModel(String? modelPath, String? textPath) async {

    try {
      // Load the TFLite model
      await Tflite.loadModel(
        model: modelPath!,
        labels: textPath!,
      ).then((value) => debugPrint("Loaded:$value"));
      debugPrint('TFLite model loaded successfully');
    } catch (e) {
      debugPrint('Failed to load TFLite model: $e');
    }
    debugPrint("models:${models.first} ${_selectModel?.name}" );

  }


  void initCamera(CameraDescription cameraDescription) async {

    // Instantiating the camera controller
    final CameraController cameraController = CameraController(
      cameraDescription,
      currentResolutionPreset,
      enableAudio: false,
      imageFormatGroup: ImageFormatGroup.bgra8888,
    );

    // Replace with the new controller
    if (mounted) {
      setState(() {
        controller = cameraController;
      });
    }

    // Update UI if controller updated
    cameraController.addListener(() {
      if (mounted) setState(() {});
    });

    // Initialize controller
    try {
      await cameraController.initialize().then((value) {
        cameraController.lockCaptureOrientation(DeviceOrientation.portraitUp);
        if (!mounted) {
          return;
        }

        setState(() {});
        _currentFlashMode = controller!.value.flashMode;

        setState(() {
          cameraController.startImageStream(
                (image) async{
              cameraImage = image;
              if(isAuto){
                var results = await compute(runModel, image);
                debugPrint('Inference results: $results');
                recognitionsList=results;
                // runModel(cameraImage);
              }
            },
          );

          debugPrint("cameraImage$cameraImage");
        });
      });
    } on CameraException catch (e) {
      if (e is CameraException) {
        switch (e.code) {
          case 'CameraAccessDenied':
            debugPrint('User denied camera access.');
            break;
          default:
            debugPrint('Handle other errors.');
            break;
        }
      }
    }

    // Update the Boolean
    if (mounted) {
      setState(() {
        _isCameraInitialized = controller!.value.isInitialized;
      });
    }
  }
  Future<List<dynamic>> runModel(CameraImage? cameraImage) async {
    setState(() {
      isAuto=true;
    });
    debugPrint("runModel");
    final runs = DateTime.now().millisecondsSinceEpoch;

    var recognitions =  await Tflite.runModelOnFrame(
      bytesList: cameraImage!.planes.map((plane) {
        return plane.bytes;
      }).toList(), // transforms the image into a bit array
      imageHeight: cameraImage.height,
      imageWidth: cameraImage.width,
      imageMean: 127.5,
      imageStd:127.5,
      numResults: 1, threshold: 0.1,
    ).then((recognitions) {
      recognitionsList=recognitions;
      recognitions?.map((res) {
        debugPrint("labelValue:${res['label']}");

      });

      debugPrint("recognitions:$recognitions");
    });
    debugPrint("recognitionsList$recognitionsList");
    final run = DateTime.now().millisecondsSinceEpoch - runs;

    debugPrint('Time to run inference: $run ms');
    setState(() {
      cameraImage;
    });
    return recognitions;

  }

  @override
  void dispose() {
    super.dispose();
    isAuto=false;
    Tflite.close();
    controller?.dispose();
    NetworkUtils.stopTimer();

  }

  @override
  Widget build(BuildContext context) {
    final MQTTAppState appState = Provider.of<MQTTAppState>(context);
    // Keep a reference to the app state.
    currentAppState = appState;
    debugPrint("appState:${currentAppState?.getAppConnectionState}");

    Size size = MediaQuery.of(context).size;

    return Container(
        color: primaryColor,
        child: SafeArea(
            child: Scaffold(
              backgroundColor: whiteColor,
              body: SlidingUpPanel(
                defaultPanelState: PanelState.CLOSED,
                minHeight: 60,
                maxHeight: 550,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(18.0),
                    topRight: Radius.circular(18.0)),
                parallaxEnabled: true,
                panelBuilder: (scrollController) => buildSlidingPanel(
                    scrollController: scrollController, size: size),
                body: Stack(
                  children: [
                    (!controller!.value.isInitialized)
                        ? Container()
                        :Positioned(
                        top: 0.0,
                        left: 0.0,
                        width: size.width,
                        height: size.height,
                        child: SizedBox(
                            width: size.width,
                            height: size.height / controller!.value.aspectRatio,
                            child: AspectRatio(
                              aspectRatio: controller!.value.aspectRatio,
                              child: CameraPreview(controller!),
                            ))
                    ),
                    const Align(
                        alignment: Alignment.topLeft,
                        child: AppBarWidget(
                          text: selfTxt,
                          color: whiteColor,
                        )),
                  ],
                ),
              ),
            )));
  }
  Future<void> toggleIsData() async {
    // isData = !isData;
    if (isBot==true) {
      _configureAndConnect();
    } else {
      setState(() {
        controlValue="0,0";
        valueController.text="";
        valueController.clear();
      });
      _publishMessage(controlValue);
      _client?.unsubscribe(topicValue!);

    }
  }

  buildSlidingPanel(
      {required ScrollController scrollController, required Size size}) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(40),
        child: AppBar(
          title: const Center(
              child: Icon(
                Icons.drag_handle,
                color: darkColor,
              )),
          automaticallyImplyLeading: false,
          backgroundColor: whiteColor,
          elevation: 0,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0))),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: size.width / 2.2,
                    child: CupertinoWidget(
                      text: autoTxt,
                      isValue: isAuto,
                      onChange: (bool? val) {
                        setState(() {
                          isAuto = val!;
                        });
                        // initCamera(controller!.description);
                        if(isAuto==false){
                          setState(() {
                            recognitionsList=[];
                            recognitionsList?.clear();
                            isAuto = false;

                          });

                        }else{
                          initCamera(controller!.description);
                          setState(() {
                            isAuto = true;
                          });                        }

                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: size.width / 2.2,
                    child: CupertinoWidget(
                      text: botTxt,
                      isValue: isBot,
                      onChange: currentAppState?.getAppConnectionState == MQTTAppConnectionState.connected ?(bool? val) {
                        setState(() {
                          isBot = val!;
                        });
                        toggleIsData();
                      }:null,
                    ),
                  ),
                )
              ]),
          Padding(
            padding: const EdgeInsets.only(top: 8, left: 32, right: 16),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: size.width / 3,
                    child: const TextWidget(
                        text: modelTxt,
                        size: 16,
                        maxLines: 2,
                        color: darkColor,
                        weight: FontWeight.w500),
                  ),
                  SizedBox(
                    width: size.width / 2,
                    height: size.height / 12,
                    child: InputDecorator(
                      decoration: const InputDecoration(border: OutlineInputBorder(borderSide: BorderSide(color: lightGreyColor),),contentPadding: EdgeInsets.only(left: 16,right: 16)),
                      child:DropdownButtonHideUnderline(
                        child: DropdownButton<PlatformFile>(
                          value: _selectModel,
                          onChanged: (value) {
                            setState(() {
                              _selectModel = value;
                              debugPrint("selectModel:${_selectModel!.path}");
                              // loadModel(_selectModel!.path,_selectModel?.path!.replaceAll(".tflite", ".txt"));
                              loadModel("assets/selfdriving.tflite","assets/selfdriving.txt");
                            });
                          },
                          hint: const TextWidget(
                              text: "Select Model",
                              size: 13,
                              maxLines: 2,
                              align: TextAlign.start,
                              color: darkColor,
                              weight: FontWeight.w400),
                          // Hide the default underline
                          underline: Container(),
                          // set the color of the dropdown menu
                          dropdownColor: whiteColor,
                          iconEnabledColor: primaryColor,

                          isExpanded: true,

                          // The list of options
                          items: models
                              .map((e) => DropdownMenuItem<PlatformFile>(
                            value: e,
                            child: Container(
                              alignment: Alignment.centerLeft,
                              child: TextWidget(
                                  text: e.name,
                                  size: 14,
                                  maxLines: 2,
                                  align: TextAlign.start,
                                  color: primaryColor,
                                  weight: FontWeight.w500),
                            ),
                          ))
                              .toList(),
                        ),
                      ),

                    ),
                  ),
                ]),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 32, right: 8),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  const TextWidget(
                      text: predictTxt,
                      size: 16,
                      color: darkColor,
                      weight: FontWeight.w500),
                  _contentWidget(),
                ]),
          ),

          Padding(
            padding: const EdgeInsets.only( left: 32,right: 8),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const TextWidget(
                      text: controlTxt,
                      size: 16,
                      color: darkColor,
                      weight: FontWeight.w500),
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0, top: 16, left: 24),
                    child: SizedBox(
                      width: size.width / 2,
                      height: size.height / 12,
                      child: TextFormField(
                        controller: valueController,
                        textInputAction: TextInputAction.done,
                        decoration: const InputDecoration(
                            contentPadding: EdgeInsets.only(left: 8),
                            border: OutlineInputBorder(borderSide: BorderSide(color: lightGreyColor),),
                            fillColor: lightGreyColor),
                        keyboardType: TextInputType.text,
                        onSaved: (String? val) {
                          setState(() {
                            valueController.text = val!;
                            // loginOTPRequest?.username = val!;
                            // mobileNo=val;
                          });
                        },
                      ),
                    ),
                  ),
                ]),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: SizedBox(
              width: size.width / 1.8,
              child: CupertinoWidget(
                text: motorTxt,
                isValue: isMotor,
                onChange: (bool? val) {
                  setState(() {
                    isMotor = val!;
                  });
                },
              ),
            ),
          )
        ],
      ),
    );
  }

  _configureAndConnect() {
    debugPrint("IP__$ipAdd  $usrName $pass");
    debugPrint("Calling");
    // ignore: flutter_style_todos
    // TODO: Use UUID
    String osPrefix = 'Flutter_iOS';
    manager = MQTTManager(
      host:ipAdd!,
      topic:topicValue!,
      identifier: osPrefix, state: currentAppState!,
    );
    manager?.initializeMQTTClient();
    manager?.connect("Cvpro","Cvpro");
  }
  void _publishMessage(String text) {
    manager?.publish(text,topicValue);
  }

  Widget _contentWidget() {
    debugPrint("ListValue${recognitionsList}");
    if ( recognitionsList==null || recognitionsList!.isEmpty) {
      return SizedBox(height: 50,);
    }

    return Padding(
      padding: const EdgeInsets.only(right: 32.0,  left: 60),
      child: Container(
        height: 30,

        child: ListView.builder(
          shrinkWrap: true,
          itemCount: recognitionsList?.length,
          scrollDirection: Axis.horizontal,

          itemBuilder: (context, index) {
            debugPrint("recognitionsList___${(recognitionsList![index]['confidence'] * 100).toStringAsFixed(0)}%}");
            var valuePer=(recognitionsList![index]['confidence'] * 100).toStringAsFixed(0);
            // double percentage=valuePer/100;
            debugPrint("labelValue:${recognitionsList![index]['label']} ${valuePer}");
            // if (valuePer > 0.1){
            Future.delayed(Duration.zero,(){
              if(currentAppState!.getAppConnectionState == MQTTAppConnectionState.connected && isBot==true){
                debugPrint("Connected");

                if( NetworkUtils.hasInternet){
                  setState(() {
                    controlValue=recognitionsList![index]['label'].toString();
                    valueController.text=controlValue;
                    _publishMessage(controlValue);
                  });



/*
                    if (recognitionsList![index]['label']=="left") { // midpoint is on the left side of the camera
                      setState(() {
                        controlValue="200,-150";
                        valueController.text=controlValue;
                        _publishMessage(controlValue);
                      });

                      // _botMovementMechanism.moveLeft();
                    } else if (recognitionsList![index]['label']=="right") { // midpoint is on the right side of the camera
                      setState(() {
                        controlValue="-150,200";
                        _publishMessage(controlValue);
                        valueController.text=controlValue;
                      });

                    } else { // midpoint is in the center of the camera
                      setState(() {
                        controlValue="200,200";
                        _publishMessage(controlValue);
                        valueController.text=controlValue;

                      });

                    }
*/
                }else{
                  snackBarAlert("Mqtt Disconnected");
                  setState(() {
                    controlValue="0,0";
                    valueController.text="";
                    valueController.clear();
                  });
                }

              }else{
                setState(() {
                  valueController.text=controlValue;
                  valueController.clear();
                });
              }
            });

            return recognitionsList![index]['label'].toString().isNotEmpty?Container(
                padding:
                const EdgeInsets.only(left: 28, right: 20,bottom: 8),
                width: 150.0,
                child: TextWidget(
                    text: recognitionsList![index]['label']??"",
                    size: 16,
                    maxLines: 1,
                    color: primaryColor,
                    weight: FontWeight.w600)):const Text("");
            // }else{
            //   return const SizedBox.shrink(); // hide the item if confidence < 0.8
            // }

          },
        ),
      ),
    );

  }

}
