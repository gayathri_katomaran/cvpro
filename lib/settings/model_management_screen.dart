

import 'dart:convert';
import 'dart:io';

import 'package:aibot_project/settings/selected_files.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:path/path.dart' as p;
import 'package:shared_preferences/shared_preferences.dart';

import '../../util/color_constants.dart';
import '../../widget/app_bar.dart';
import '../../widget/button_widget.dart';
import '../../widget/text_widget.dart';
import '../util/available_cameras.dart';

class ModelManagementScreen extends StatefulWidget {
  const ModelManagementScreen({Key? key}) : super(key: key);

  @override
  State<ModelManagementScreen> createState() => _ModelManagementScreenState();
}

class _ModelManagementScreenState extends State<ModelManagementScreen> {
  List<PlatformFile> models = [];
  List<PlatformFile> detectionList=[];
  List<PlatformFile> classifyList=[];
  List<PlatformFile> drivingList=[];
  List<PlatformFile> drivingTextList=[];
  List<SelectedFiles> selectedFiles = [];


  List<PlatformFile> tfliteModels = [];
  String fileName="",labelName="",labelPath="";
  // List<String> models=[];
  final _formEdit = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  String? _selectModel;
  bool  isDynamic=false;
  final List<String> modelList = ["Object Detection", "Image Classification","Self Driving" ];
  final List<PlatformFile> staticModels = [PlatformFile(name: "ssd_mobilenet.tflite", path: "assets/ssd_mobilenet.tflite", size: 0),
    PlatformFile(name: "ssd_mobilenet.txt",size:0,path: "assets/ssd_mobilenet.txt"),
    PlatformFile(name: "cv_pro_2.tflite", path: "assets/cv_pro_2.tflite", size: 0) ,PlatformFile(name: "cv_pro_2.txt",size:0,path: "assets/cv_pro_2.txt") ,];

  @override
  void initState() {
    super.initState();

    models.addAll(staticModels);

    if(storage.getItem("Models")!=null){
      models=storage.getItem("Models")??[];
    }
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      color: primaryColor,
      child: SafeArea(
        child:  Scaffold(
          resizeToAvoidBottomInset: false,
          body: Stack(
            fit: StackFit.expand,
            children: [
              Container(
                color: Colors.white,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child:Padding(
                  padding: const EdgeInsets.only(top:20.0,left: 16,right: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children:   [
                      const AppBarWidget(text: "Model Management"),
                  SizedBox(
                    height: MediaQuery.of(context).size.height/1.6,
                    child: ListView.builder(
                      itemCount: models.length,
                      itemBuilder: (context, index) {
                        final item = models[index];
                        final isList2Item = staticModels.contains(item);
                            debugPrint("isList2Item$isList2Item");
                        final extension = p.extension(models[index].path!);

                        return Dismissible(
                          key: Key(item.name),
                          direction: DismissDirection.startToEnd,
                          child: ListTile(
                            title:Row(
                              mainAxisAlignment:MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                        padding: const EdgeInsets.only(left: 12.0,right: 8),
                        child: TextWidget(
                          text: item.name,
                          size: 16,
                          maxLines: 2,
                          color: darkColor,
                          weight: FontWeight.w500,
                        ),
                      ),
                                Visibility(
                                  visible:isList2Item==false && extension==".tflite",
                                  child: IconButton(
                                    icon: const Icon(Icons.edit,color: primaryColor,),
                                    onPressed: () {
                                      // bottomSheetEdit(setState,models[index].name,models[index].path,models[index].size);
                                    },
                                  ),
                                ),
                              ],
                            ),
                            trailing: Visibility(
                              visible:isList2Item==false && extension==".tflite",

                              child: IconButton(
                                icon: const Icon(Icons.delete_outline,color: Colors.red,),
                                onPressed: () {
                                  setState(() {
                                    models.removeAt(index);
                                    // storage.setItem("files_list", dynamicList);

                                    // storage.setItem("", value)
                                  });
                                },
                              ),
                            ),
                          ),
                          onDismissed: (direction) {
                            setState(() {
                              models.removeAt(index);
                              models.removeAt(index);
                              // storage.setItem("files_list", dynamicList);
                            });
                          },
                        );
                      },
                    ),
                  ),
                    ],
                  ),
                ),
              ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [

                Padding(
                  padding: const EdgeInsets.only(top:8.0,bottom: 50),
                  child: ButtonWidget(
                    width: MediaQuery.of(context).size.width / 1.4,
                    text: "Upload",
                    size: 16,
                    txtColor: Colors.white,
                    borderColor: primaryColor,
                    color: primaryColor, onPressed: () {
                    _pickFile();
                  },
                  ),
                ),
              ],
            ),
          ),

            ],),
        ),

      ),
    );

  }
  void _pickFile() async {
    // opens storage to pick files and the picked file or files
    // are assigned into result and if no file is chosen result is null.
    // you can also toggle "allowMultiple" true or false depending on your need
    final result = await FilePicker.platform.pickFiles(allowMultiple: true, type: FileType.any,withData: true,withReadStream: true
      // allowedExtensions: ['tflite', 'txt'],
    );

    // if no file is picked
    if (result == null) return;

    bottomSheetEdit(setState,result.files.first.name,result.files.single.path,result.files.first.size);
    // we get the file from result object
    setState((){
      models.addAll(result.files);
      // dynamicList.addAll(result.files);

      isDynamic=true;
      storage.setItem("Models", models);

      // models=file;
    });


    tfliteModels.addAll(tfliteModels.where((element) => element.name.endsWith('.tflite')));
    if(tfliteModels.isNotEmpty){
      storage.setItem("files_list", tfliteModels);
      debugPrint("models__${tfliteModels.length}");

    }

    // _openFile(file);
  }

  void bottomSheetEdit(void setState, String name, String? path, int size) {
    nameController.text=name;
    FilePickerResult? textFile;
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        backgroundColor: whiteColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        isScrollControlled: true,
        builder: (builder) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter state) {
              setState = state;
              return SingleChildScrollView(
                child: LimitedBox(
                  child:  Padding(
                    padding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom),
                    child: Form(
                      key: _formEdit,

                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          const Padding(
                            padding: EdgeInsets.only(left: 16.0,right: 16,top: 24),
                            child: TextWidget(
                              text: "Model Details",
                              color:
                              darkColor,
                              weight:
                              FontWeight
                                  .w700,
                              size: 16,
                              maxLines: 4,
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Padding(
                            padding: const EdgeInsets.only( left: 32,right: 8),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  const TextWidget(
                                      text: "Name",
                                      size: 16,
                                      color: darkColor,
                                      weight: FontWeight.w500),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 8.0, top: 16, left: 24),
                                    child: SizedBox(
                                      width: MediaQuery.of(context).size.width / 2,
                                      height: MediaQuery.of(context).size.height / 12,
                                      child: TextFormField(
                                        controller: nameController,
                                        textInputAction: TextInputAction.done,
                                        decoration: const InputDecoration(
                                            contentPadding: EdgeInsets.only(left: 8),
                                            border: OutlineInputBorder(borderSide: BorderSide(color: lightGreyColor),),
                                            fillColor: lightGreyColor),
                                        keyboardType: TextInputType.text,
                                        onSaved: (String? val) {
                                          state(() {
                                            nameController.text = val!;
                                            // loginOTPRequest?.username = val!;
                                            // mobileNo=val;
                                          });
                                        },
                                      ),
                                    ),
                                  ),
                                ]),
                          ),

                          Padding(
                            padding: const EdgeInsets.only( top: 8,left: 32,right: 16),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,

                                children: [
                                  SizedBox(
                                    width: MediaQuery.of(context).size.width / 3,

                                    child: const TextWidget(
                                        text: "Type",
                                        size: 16,
                                        maxLines: 2,
                                        color: darkColor,
                                        weight: FontWeight.w500),
                                  ),
                      SizedBox(
                        width:  MediaQuery.of(context).size.width / 2,
                        height:  MediaQuery.of(context).size.height / 12,
                        child: InputDecorator(
                          decoration: const InputDecoration(border: OutlineInputBorder(borderSide: BorderSide(color: lightGreyColor),),contentPadding: EdgeInsets.only(left: 16,right: 16)),
                          child:DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                              value: _selectModel,
                              onChanged: (value) async{
                                state(() {
                                  _selectModel = value;
                                  debugPrint("_selectModel:$_selectModel");
                                  if(_selectModel=="Object Detection"){
                                    detectionList.add(PlatformFile(name: name, size: size,path: path!));

                                    saveObject(detectionList,"Detector",name,path,labelName,labelPath);

                                    storage.setItem("Detector", detectionList);
                                  }else if(_selectModel=="Image Classification"){
                                    classifyList.add(PlatformFile(name: name, size: size,path: path!));
                                    storage.setItem("Classify", classifyList);
                                  }else if(_selectModel=="Self Driving"){
                                    drivingList.add(PlatformFile(name: name, size: size,path: path!));
                                    drivingTextList.add(PlatformFile(name: labelName, size: size,path: labelPath));

                                    saveObject(drivingList,"Driving",name,path,labelName,labelPath);

                                    storage.setItem("Driving", drivingList);
                                    storage.setItem("DrivingText", drivingTextList);
                                  }
                                });
                              },
                              hint: const TextWidget(
                                  text: "Select Model",
                                  size: 13,
                                  maxLines: 2,
                                  align: TextAlign.start,
                                  color: darkColor,
                                  weight: FontWeight.w400),
                              // Hide the default underline
                              underline: Container(),
                              // set the color of the dropdown menu
                              dropdownColor: Colors.white,
                              iconEnabledColor: primaryColor,

                              isExpanded: true,

                              // The list of options
                              items: modelList
                                  .map((e) => DropdownMenuItem(
                                value: e,
                                child: Container(
                                  alignment: Alignment.centerLeft,
                                  child: TextWidget(
                                      text: e,
                                      size: 14,
                                      maxLines: 2,
                                      align: TextAlign.start,
                                      color: primaryColor,
                                      weight: FontWeight.w500),
                                ),
                              ))
                                  .toList(),
                            ),
                          ),

                        ),
                      ),
                                ]),
                          ),

                          Padding(
                            padding: const EdgeInsets.only( left: 32,right: 8),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  InkWell(
                                    onTap: ()async{
                                       textFile = await FilePicker.platform.pickFiles(type: FileType.custom, allowedExtensions: ['txt'],withData: true,withReadStream: true,allowMultiple: false);

                                      if (textFile != null) {
                                        labelName=textFile?.files.first.name??"";
                                        PlatformFile file = textFile!.files.first;
                                        state((){
                                          labelName=file.name;
                                          File readFile=File(file.path!);
                                          readFile.readAsString();
                                          labelPath=readFile.path;
                                        });
                                        // Navigator.pop(context, File(textFile.files.single.path!));
                                      }
                                      },
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: const [
                                        TextWidget(
                                            text: "Name",
                                            size: 16,
                                            color: darkColor,
                                            weight: FontWeight.w500),
                              Icon(Icons.attach_file),
                                      ],
                                    ),
                                  ),

                              Padding(
                                padding: const EdgeInsets.only(left: 12.0,right: 8),
                                child: TextWidget(
                                  text: labelName,
                                  size: 16,
                                  maxLines: 2,
                                  color: darkColor,
                                  weight: FontWeight.w500,
                                ),
                              ),
                                ]),
                          ),
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.only(top:16.0,bottom: 16,left: 16,right: 16),
                              child: ButtonWidget(
                                width: MediaQuery.of(context).size.width / 1,
                                text: "Sumbit",
                                size: 16,
                                txtColor: whiteColor,
                                borderColor: primaryColor,
                                color: primaryColor, onPressed: () async{
                             /*   if (_formEdit.currentState!.validate()) {
                                  _formEdit.currentState!.save();
                                  state((){
                                    name=nameController.text;
                                  });
                                  Get.back();
                                }*/

                                if (textFile == null) {
                                  return;
                                }

                                final textFilePath = textFile?.files.first.path;
                                final tfliteFileName = p.basenameWithoutExtension(path!);

                                final textFileBaseName = File(textFilePath!).uri.pathSegments.last;
                                final textFileWithoutExtension =
                                textFileBaseName.replaceFirst(RegExp(r'\.txt$'), '');
                                final newTextFilePath = textFilePath.replaceFirst(textFileBaseName, '$tfliteFileName.txt');
                                await File(textFilePath).copy(newTextFilePath);

                                state(() {
                                  models.add(PlatformFile(name: "$tfliteFileName.txt", size: size,path: newTextFilePath));
                                    // models.addAll(textFile!.files);
                                });
                                Get.back(result: 1);
                                state(() {
                                });
                              },
                              ),
                            ),
                          )

                        ],
                      ),
                    ),
                  ),
                ),
              );
            },
          );
        });

  }
  saveObject(List<PlatformFile> detectionList, String type, String name, String path, String labelName, String labelPath) async{
    Map<String, String> fileMap = {};
    String fileContent = '';

    for (PlatformFile file in detectionList) {
      String fileName = file.name.replaceAll('.tflite', '').replaceAll('.txt', '');
      String? filePath = file.path;

      fileMap[fileName] = filePath!;
      fileContent = await File(fileMap[fileName]!).readAsString();

    }

    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(type, jsonEncode(fileMap));


    final selectedFile = SelectedFiles(
      tfliteName: name??"",
      tflitePath: path ?? '',
      textName: labelName??"",
      textPath: labelPath ?? '',
    );

    setState(() {
      selectedFiles.add(selectedFile);
    });

    // Save the list of selected files in shared preferences
    final sharedPrefs = await SharedPreferences.getInstance();
    final selectedFilesJson =
    selectedFiles.map((e) => json.encode(e.toJson())).toList();
    sharedPrefs.setStringList('selectedFiles', selectedFilesJson);
  }
}
