class SelectedFiles {
   String tfliteName;
   String tflitePath;
   String textName;
   String textPath;

  SelectedFiles({
    required this.tfliteName,
    required this.tflitePath,
    required this.textName,
    required this.textPath,
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['tfliteName'] = tfliteName;
    data['tflitePath'] = tflitePath;
    data['textName'] = textName;
    data['textPath'] = textPath;
    return data;
  }

  factory SelectedFiles.fromJson(Map<String, dynamic> json) {
    return SelectedFiles(
      tfliteName: json['tfliteName'],
      tflitePath: json['tflitePath'],
      textName: json['textName'],
      textPath: json['textPath'],
    );
  }
}