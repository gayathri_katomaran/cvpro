import 'dart:async';

import 'package:aibot_project/widget/text_widget.dart';
import 'package:app_settings/app_settings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../util/MQTTManager.dart';
import '../util/available_cameras.dart';
import '../util/color_constants.dart';
import '../util/image_constants.dart';
import '../util/state/MQTTAppState.dart';
import '../util/string_constants.dart';
import '../widget/app_bar.dart';
import '../widget/cupertino_switch.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> with WidgetsBindingObserver{
  bool isBot = false;
  bool isCamera = false;
  bool isStorage = false;
  MqttClient? client;
  MQTTAppState? currentAppState;
  bool mqttConnected = false;
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  MQTTManager? manager;

  StreamSubscription? subscription;

  @override
  void initState() {
    super.initState();


    checkMQTTConnection();
    _loadCameraPermission();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      _loadCameraPermission();
    }
  }
  Future<void> checkMQTTConnection() async {


    SharedPreferences sharedPreferences = await _prefs;
    mqttConnected = sharedPreferences.getBool('connected') ?? false;

    // Check MQTT connection status
    if (mqttConnected) {
      setState(() {
        isBot = true;
      });
    } else {
      setState(() {
        isBot = false;
      });
    }
  }

  Future<void> _loadCameraPermission() async {
    bool _cameraPermission = await Permission.camera.isGranted;
    setState(() {
      cameraPermission = _cameraPermission;
    });
  }


/*
  void _onSwitchChanged(bool? value) async{
    SharedPreferences sharedPreferences = await _prefs;

    setState(() {
      isBot = value!;
    });
    if (isBot) {
      setState(() {
        sharedPreferences.setBool("connected",true);

      });
      Get.toNamed('/connect')?.then((value) async {
        debugPrint("value$value");
        if(value==1){
          setState(() {
            isBot=true;
            sharedPreferences.setBool("connected",true);

          });
        }else {
          setState(() {
            isBot = false;
            sharedPreferences.setBool("connected",false);

          });
        }
      });


*/
/*      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => AnotherScreen()),
      ).then((value) {
        if (_mqttConnected) {
          setState(() {
            _toggleValue = true;
          });
        } else {
          setState(() {
            _toggleValue = false;
          });
        }
      });*//*

      // Connect to MQTT
    } else {
      // Disconnect from MQTT
      setState(() {
        debugPrint("DisConnect");
        manager?.disconnect();
        sharedPreferences.setBool("connected",false);
      });
    }
  }
*/

  void _onSwitchChanged(bool? value) async{
    SharedPreferences sharedPreferences = await _prefs;

    setState(() {
      isBot = value!;
    });
 debugPrint("isBot:$isBot");
    Get.toNamed('/connect')?.then((value) async {
      debugPrint("value$value");
      if(value==1){
        setState(() {
          isBot=true;
          sharedPreferences.setBool("connected",true);
        });
      }else {
        setState(() {
          isBot = false;
          sharedPreferences.setBool("connected",false);

        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final MQTTAppState appState = Provider.of<MQTTAppState>(context);
    // Keep a reference to the app state.
    currentAppState = appState;
    debugPrint("appState:${currentAppState?.getAppConnectionState}");

    // if(currentAppState?.getAppConnectionState == MQTTAppConnectionState.connected){
    //   isBot=true;
    // }else{
    //   isBot=false;
    // }

    return Container(color:primaryColor,child: SafeArea(child: Scaffold(
      backgroundColor: whiteColor,
      body: Container(
        color: whiteColor,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children:   [
            const AppBarWidget(text: settingsTxt),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: CupertinoWidget( text: connectTxt, isValue: isBot, onChange: _onSwitchChanged
            /*      (bool? val ) { setState(() {
                isBot=val!;
              });
              if(isBot==true){
                Get.toNamed('/connect');
              }
              // else{
              //   _disconnect();
              // }
                }*/
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: modelManage(),
            ),
            const Padding(
              padding: EdgeInsets.only(left: 24.0,right: 16,top: 8),
              child: TextWidget(text: permissionTxt, size: 20, color: darkColor, weight: FontWeight.w600),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: CupertinoWidget( text: cameraTxt, isValue: cameraPermission, onChange: (bool? val ) async{
                setState(() {
                  cameraPermission = val!;
                });
                if (cameraPermission) {
                  await Permission.camera.request();
                } else {
                  // await Permission.camera.revoke();
                  AppSettings.openAppSettings();

                  // MyClass().openSystemPermissionsSettings();
                  // await openAppSettings();
                }
                // var status = await Permission.camera.status;
                // setState(() {
                //   _cameraPermissionStatus = status;
                // });
              },),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: CupertinoWidget( text: storageTxt, isValue: isStorage, onChange: (bool? val ) { setState(() {
                isStorage=val!;
              }); },),
            ),
          ],
        ),
      ),
    ),

    ),);
  }
  Widget modelManage(){
    return InkWell(
      onTap: (){
        Get.toNamed('/model');
        // Get.to(MyHomePage());
      },
      child: Padding(
        padding: const EdgeInsets.only(left: 12.0,right: 16,top: 16,bottom: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Padding(
              padding: EdgeInsets.only(left: 12.0,right: 8),
              child: TextWidget(
                text: manageTxt,
                size: 16,
                color: darkColor,
                weight: FontWeight.w500,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 16.0),
              child: SvgPicture.asset(next,fit: BoxFit.contain,width: 15,height: 15,),
            )
          ],
        ),
      ),
    );
  }

  Future<void> disconnect() async {
    if (client != null && client?.connectionStatus?.state == MqttConnectionState.connected) {
       client?.disconnect();
    }
  }

}
