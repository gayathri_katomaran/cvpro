import 'package:aibot_project/util/state/MQTTAppState.dart';
import 'package:flutter/material.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';

class MQTTManager {
  // Private instance of client
  final MQTTAppState _currentState;
  MqttServerClient? _client;
  final String _identifier;
  final String _host;
  final String _topic;

  // Constructor
  // ignore: sort_constructors_first
  MQTTManager(
      {required String host,
      required String topic,
      required String identifier,
      required MQTTAppState state})
      : _identifier = identifier,
        _host = host,
        _topic = topic,
        _currentState = state;

  void initializeMQTTClient() {
    _client = MqttServerClient(_host, '');
    _client!.port = 1883;
    _client!.keepAlivePeriod = 60;
    // _client!.onDisconnected = onDisconnected;
    // _client!.secure = false;
    _client!.logging(on: true);

    /// Add the successful connection callback
    _client!.onConnected = onConnected;
    _client!.onSubscribed = onSubscribed;
    _client!.pongCallback = pong;

    final MqttConnectMessage connMess = MqttConnectMessage()
        .withClientIdentifier(_identifier)
        .withWillTopic(
            _topic) // If you set this you must set a will message
        .withWillMessage('My Will message')
        .startClean() // Non persistent session for testing
        .withWillQos(MqttQos.exactlyOnce);
    debugPrint('EXAMPLE::Mosquitto client connecting....');
    _client!.connectionMessage = connMess;
  }
  void pong() {
    debugPrint('MQTTClient::Ping response received');
  }
  // Connect to the host
  // ignore: avoid_void_async
  void connect(String userName, String password) async {

    assert(_client != null);
    try {
      debugPrint('EXAMPLE::Mosquitto start client connecting....');
      _currentState.setAppConnectionState(MQTTAppConnectionState.connecting);
      await _client?.connect(userName, password).then((value) async => debugPrint("Connected with mqtt:$value"));
      subscribeTopic();
    } on Exception catch (e) {
      debugPrint('EXAMPLE::client exception - $e');
      _client?.disconnect();
      // disconnect();
    }
  }
  subscribeTopic(){
    _client!.subscribe(_topic, MqttQos.exactlyOnce);
    debugPrint("subscribe");
  }
   disconnect() {
    debugPrint('Disconnected');
    _client?.disconnect();
    _currentState.setAppConnectionState(MQTTAppConnectionState.disconnected);
   }

  void publish(String message, String? topicValue) {
    debugPrint("Publishing");
    final MqttClientPayloadBuilder builder = MqttClientPayloadBuilder();
    builder.addString(message);
    debugPrint("publish_${ builder.payload!}");
    _client?.publishMessage(topicValue!, MqttQos.exactlyOnce, builder.payload!);
  }

  /// The subscribed callback
  void onSubscribed(String topic) {
    print('EXAMPLE::Subscription confirmed for topic $topic');
  }
  void unSubscribed() {
_client?.unsubscribe(_topic);
  }
  /// The unsolicited disconnect callback
  void onDisconnected() {
    print('EXAMPLE::OnDisconnected client callback - Client disconnection');
    if (_client!.connectionStatus!.returnCode ==
        MqttConnectReturnCode.noneSpecified) {
      print('EXAMPLE::OnDisconnected callback is solicited, this is correct');
    }
    _currentState.setAppConnectionState(MQTTAppConnectionState.disconnected);
  }

  /// The successful connect callback
  void onConnected() {
    _currentState.setAppConnectionState(MQTTAppConnectionState.connected);
    print('EXAMPLE::Mosquitto client connected....');
    _client!.subscribe(_topic, MqttQos.exactlyOnce);
    _client!.updates!.listen((List<MqttReceivedMessage<MqttMessage?>>? c) {
      // ignore: avoid_as
      final MqttPublishMessage recMess = c![0].payload as MqttPublishMessage;

      // final MqttPublishMessage recMess = c![0].payload;
      final String pt =
          MqttPublishPayload.bytesToStringAsString(recMess.payload.message);
      _currentState.setReceivedText(pt);
      print(
          'EXAMPLE::Change notification:: topic is <${c[0].topic}>, payload is <-- $pt -->');
      print('Message:${recMess.payload.message}');
    });
    print(
        'EXAMPLE::OnConnected client callback - Client connection was sucessful');
  }
}
