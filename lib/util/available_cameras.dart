
import 'dart:async';

import 'package:camera/camera.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:http/http.dart' as http;
import 'package:permission_handler/permission_handler.dart';

List<CameraDescription>? cameras;
final LocalStorage storage =  LocalStorage('Ai_bot');
bool cameraPermission=false;

availableCamera()async {
  try {
    cameras = await availableCameras();
  } on CameraException catch (e) {
    debugPrint('Error: $e.code\nError Message: $e.message');
  }
}

class ConnectivityService {
  Future<String> checkConnectivity() async {
    var connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.mobile) {
      return 'Mobile data';
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return 'WiFi';
    } else {
      return 'No internet';
    }
  }
}

class NetworkUtils {
  static bool isConnected = false;
  static bool hasInternet = false;
  static Timer? timer;

  static Future<void> checkConnectivity() async {
    var connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      isConnected = true;
    } else {
      isConnected = false;
    }
  }

  static Future<void> checkInternetConnection() async {
    try {
      final response = await http.get(Uri.parse('https://www.google.com'));
      if (response.statusCode == 200) {
        hasInternet = true;
      } else {
        hasInternet = false;
      }
    } catch (e) {
      hasInternet = false;
    }
  }

  static void startTimer() {
    timer = Timer.periodic(const Duration(seconds: 3), (timer) {
      checkInternetConnection();
    });
  }

  static void stopTimer() {
    timer?.cancel();
  }
}
Future<bool> requestPermission() async {

  bool storagePermission = await Permission.storage.isGranted;
  bool mediaPermission = await Permission.accessMediaLocation.isGranted;
  bool manageExternal = await Permission.manageExternalStorage.isGranted;

  if (!storagePermission) {
    storagePermission = await Permission.storage.request().isGranted;
  }

  if (!mediaPermission) {
    mediaPermission =
    await Permission.accessMediaLocation.request().isGranted;
  }

  if (!manageExternal) {
    manageExternal =
    await Permission.manageExternalStorage.request().isGranted;
  }

  bool isPermissionGranted =
      storagePermission && mediaPermission && manageExternal;

  if (isPermissionGranted) {
    return true;
  } else {
    return false;
  }}




