import 'package:shared_preferences/shared_preferences.dart';

class SharedPref {
  var files = "FILES";
  var model = "MODEL_PATH";
  var txtPath = "TEXT_PATH";
  var cameraOn = "CAMERA_ON";
  var ipAddress = "IP_ADDRESS";
  var userName = "USER_NAME";
  var password = "PASSWORD";



  setStringPref(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
    return true;
  }

  getStringPref(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key) ?? "";
  }

  setIntegerPref(String key, int value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt(key, value);
    return true;
  }

  getIntegerPref(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(key) ?? "";
  }

setBoolPref(String key, bool value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setBool(key, value);
  return true;
}

getBoolPref(String key) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getBool(key) ?? false;
}
}
