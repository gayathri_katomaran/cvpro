
const String appName = "AI Bot";
const String dataTxt = "Data Collection";
const String objectTxt = "Object Detection";
const String classifyTxt = "Image Classification";
const String platoonTxt = "Platoon";
const String selfTxt = "Self Driving";
const String settingsTxt = "Setting";
const String permissionTxt = "Permissions";
const String connectTxt = "Bot Connection";
const String connectBtn = "Connect";
const String manageTxt = "Model Management";
const String baudTxt = "Baud Rate";
const String cameraTxt = "Camera";
const String botTxt = "Bot";
const String flashTxt = "Flash";
const String controlTxt = "Control";
const String predictTxt = "Predict";
const String resolutionTxt = "Preview Resolution";
const String motorTxt = "Motor Swap";
const String data= "Data";
const String modelTxt= "Model";
const String objectText= "Object";
const String confidentTxt= "Confidence";
const String storageTxt = "Storage";
const String wideTxt = "Wide";
const String primaryTxt = "Primary";
const String zoomTxt = "Zoom";
const String autoTxt = "Auto";
const String detectTxt = "Detect";

const String hintIp = "Ip Address";
const String hintName = "User Name";

String userName = "Cvpro";
String password = "Cvpro";

