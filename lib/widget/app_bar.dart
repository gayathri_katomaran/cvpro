
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';


import '../util/color_constants.dart';
import '../util/image_constants.dart';
import 'text_widget.dart';

class AppBarWidget extends StatefulWidget {
  final String text;
  final Color color;
  const AppBarWidget({
    Key? key,
    required this.text,  this.color=darkColor,
  }) : super(key: key);

  @override
  AppBarWidgetState createState() => AppBarWidgetState();
}

class AppBarWidgetState extends State<AppBarWidget> {
  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InkWell(
              highlightColor: Colors.transparent,
                onTap: (){
                  Get.back();
                }
                ,child: SvgPicture.asset(backBtn,fit: BoxFit.contain,)),
          ),
           Padding(
            padding: const EdgeInsets.only(top:12.0,left: 8,right: 8,bottom: 8),
            child: TextWidget(
              text: widget.text,
              size: 22,
              color: widget.color,
              weight: FontWeight.w800,
              maxLines: 2,
            ),
          ),
        ],
      ),
    );

  }
}
