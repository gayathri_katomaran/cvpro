
import 'package:aibot_project/util/color_constants.dart';
import 'package:flutter/material.dart';

import 'text_widget.dart';

class ButtonWidget extends StatefulWidget {
  final VoidCallback? onPressed;
  final String text;
  final int size;
  final Color? color;
  final Color txtColor;
  final double? width,height;
  final Color  borderColor;


  const ButtonWidget({
    Key? key,
    required this.text,
    required this.size,
    this.color,
    required this.width,
    required this.txtColor,
    required this.onPressed,
    this.borderColor=whiteColor, this.height=48
  }) : super(key: key);

  @override
  ButtonState createState() => ButtonState();
}

class ButtonState extends State<ButtonWidget> {
  @override
  Widget build(BuildContext context) {

    return SizedBox(
      width: widget.width!.toDouble(),
      height: widget.height!.toDouble(),
      child: ElevatedButton(
        onPressed: widget.onPressed,
        style: ElevatedButton.styleFrom(
          elevation: 5, backgroundColor: widget.color,
          shadowColor: Colors.transparent.withOpacity(0.1),
          side:  BorderSide(
            width: 1,
            color: widget.borderColor,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
        ),
        child:TextWidget(
          text: widget.text,
          size: widget.size,
          color: widget.txtColor,
          weight: FontWeight.w600,
        ),
      ),
    );

  }
}
