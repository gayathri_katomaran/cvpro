
import 'package:aibot_project/widget/text_widget.dart';
import 'package:flutter/cupertino.dart';

import '../util/color_constants.dart';

class CupertinoWidget extends StatefulWidget {
  final String text;
  bool isValue;
  final Function(bool?)? onChange;


  CupertinoWidget(
      {Key? key,
        required this.text, required this.isValue, required this.onChange,})
      : super(key: key);

  @override
  CupertinoWidgetState createState() => CupertinoWidgetState();
}

class CupertinoWidgetState extends State<CupertinoWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 24.0,right: 16,top: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          TextWidget(text: widget.text, size: 16, color: darkColor, weight: FontWeight.w500),

          Transform.scale(
              scale: 0.9,
              child: CupertinoSwitch(
                value: widget.isValue,
                activeColor: primaryColor,
                onChanged: widget.onChange,
              ))
        ],
      ),
    );
  }
}
