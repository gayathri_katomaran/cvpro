import 'dart:ui';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import '../util/color_constants.dart';


 snackBarAlert(String message) async {
  Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: darkColor,
      textColor: Get.isDarkMode?const Color(0xFF2B2C43): const Color(0xffffffff));

}
