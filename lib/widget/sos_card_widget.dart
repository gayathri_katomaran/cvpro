
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../util/image_constants.dart';
import 'text_widget.dart';


class SosCard extends StatefulWidget {
  final VoidCallback onPressed;
  final String text,iconImage;
  final Color txtColor,color1,color2;

  const SosCard({
    Key? key,
    required this.text,
    required this.txtColor,
    required this.onPressed, required this.color1, required this.color2, required this.iconImage,
  }) : super(key: key);

  @override
  SosCardState createState() => SosCardState();
}

class SosCardState extends State<SosCard> {
  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: widget.onPressed,
      child: SizedBox(
        width: MediaQuery.of(context).size.width / 2.4,
        height: MediaQuery.of(context).size.height / 5.4,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          elevation: 1.0,

          child: Container(
          decoration:  BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
    gradient: LinearGradient(
    colors: [
    widget.color1,
   widget.color2
    ],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 6.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SvgPicture.asset(widget.iconImage,fit: BoxFit.contain,width: 65,height: 65,color: Colors.white,),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: SvgPicture.asset(arrow,fit: BoxFit.contain,width: 12,height: 12,),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0,right: 8,bottom: 12),
                  child: TextWidget(
                    text: widget.text,
                    size: 15,
                    maxLines: 2,
                    align: TextAlign.start,
                    color: widget.txtColor,
                    weight: FontWeight.w700,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
