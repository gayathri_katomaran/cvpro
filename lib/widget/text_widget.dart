
import 'package:flutter/material.dart';

class TextWidget extends StatefulWidget {
  final String text;
  final int size;
  final Color color;
  final FontWeight weight;
  final int maxLines;
  final TextAlign align;
  final String font;

  const TextWidget(
      {Key? key,
      required this.text,
      required this.size,
      required this.color,
      required this.weight,
      this.maxLines = 1,
        this.align=TextAlign.start,  this.font="Montserrat"})
      : super(key: key);

  @override
  TextWidgetState createState() => TextWidgetState();
}

class TextWidgetState extends State<TextWidget> {
  @override
  Widget build(BuildContext context) {
    return Text(
      widget.text,
      maxLines: widget.maxLines,
      overflow: TextOverflow.ellipsis,
      textAlign: widget.align,
      style: TextStyle(
          fontFamily: widget.font,
          fontSize: widget.size.toDouble(),
          fontStyle: FontStyle.normal,
          color: widget.color,
          fontWeight: widget.weight,
          decoration: TextDecoration.none),
      softWrap: true,
    );
  }
}
